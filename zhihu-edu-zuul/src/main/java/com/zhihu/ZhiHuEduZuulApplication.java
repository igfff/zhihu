package com.zhihu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

/**
 * @author : zhangzheng
 * ClassName: ZhiHuEduZuulApplication
 * @date : 2021/9/2 20:13
 */
@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
@EnableDiscoveryClient
@EnableZuulProxy
public class ZhiHuEduZuulApplication {
    public static void main(String[] args) {
        SpringApplication.run(ZhiHuEduZuulApplication.class);
    }
}
