package com.zhihu.filter;

import com.alibaba.fastjson.JSONObject;
import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;
import com.zhihu.pojo.baseVo.BaseResult;
import com.zhihu.utils.JwtUtils;
import com.zhihu.utils.RedisUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author : zhangzheng
 * ClassName: MyZuulFilter
 * @date : 2021/9/2 20:15
 */
@Component
public class MyZuulFilter extends ZuulFilter {
    private static final List<String> urlList=new ArrayList<>();
    private static final RedisUtils redisUtils = new RedisUtils();

    @Override
    public String filterType() {
        return "pre";
    }

    @Override
    public int filterOrder() {
        return 0;
    }

    @Override
    public boolean shouldFilter() {
        //哪些请求直接放行
        RequestContext currentContext = RequestContext.getCurrentContext();
        HttpServletRequest request = currentContext.getRequest();
        String requestURI = request.getRequestURI();
        if (urlList.contains(requestURI)){
            return  false;
        }
        return true;
    }

    @Override
    public Object run() throws ZuulException {
        //进入过滤器，通过request获取到header
        RequestContext currentContext = RequestContext.getCurrentContext();
        HttpServletRequest request = currentContext.getRequest();
        String token = request.getHeader("token");
        if (token!=null){
            //从redis中获取到 token
            boolean b = redisUtils.hasKey(token);
            if (b){
                Object o = redisUtils.get(token);
                //判断用户的权限等等等。。。
                JwtUtils jwtUtils = new JwtUtils();
                try {
                    jwtUtils.VerifyToekn(o.toString());
                }catch (Exception e){
                    HttpServletResponse response = currentContext.getResponse();
                    response.setCharacterEncoding("utf-8");
                    response.setContentType("application/json");
                    try {
                        response.getWriter().write(JSONObject.toJSONString(new BaseResult(0,"登录信息已失效！")));
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }finally {
                        currentContext.setSendZuulResponse(false);
                    }
                }

            }else{
                HttpServletResponse response = currentContext.getResponse();
                response.setCharacterEncoding("utf-8");
                response.setContentType("application/json");
                try {
                    response.getWriter().write(JSONObject.toJSONString(new BaseResult(0,"用户token异常！")));
                } catch (IOException e) {
                    e.printStackTrace();
                }finally {
                    currentContext.setSendZuulResponse(false);
                }
            }


        }else{
            HttpServletResponse response = currentContext.getResponse();
            response.setCharacterEncoding("utf-8");
            response.setContentType("application/json");
            try {
                response.getWriter().write(JSONObject.toJSONString(new BaseResult(0,"请登录")));
            } catch (IOException e) {
                e.printStackTrace();
            }finally {
                currentContext.setSendZuulResponse(false);
            }
        }
        return null;
    }

    public MyZuulFilter(){
        urlList.add("/zhihu-edu-question/question/findQuestionById");
        urlList.add("/zhihu-edu-answer/answer/findAnswersByQuestionId");
        urlList.add("/zhihu-edu-comments/comments/findCommentsLikeNumSort");
        urlList.add("/zhihu-edu-comments/comments/findCommentsTimeSort");
        urlList.add("/zhihu-edu-opn/opn/getOpusCommNumsFromRedis");
        urlList.add("/zhihu-edu-index/index/findIndexList");
        urlList.add("/zhihu-edu-like/like/getOpusLikeNums");
        urlList.add("/zhihu-edu-answer/answer/findAnswersByQuestionId");
        urlList.add("/zhihu-edu-user/user/decideTel");
        urlList.add("/zhihu-edu-user/user/getCode");
        urlList.add("/zhihu-edu-user/user/registry");
        urlList.add("/zhihu-edu-user/user/login");
        urlList.add("/zhihu-edu-user/user/loginMsg");
        urlList.add("/zhihu-edu-user/user/findUserByToken");
        urlList.add("/zhihu-edu-answer/answer/addAnswers");
        urlList.add("/zhihu-edu-video/video/recommendVideos");
        urlList.add("/zhihu-edu-video/video/findVideoByVideoId");
    }
}
