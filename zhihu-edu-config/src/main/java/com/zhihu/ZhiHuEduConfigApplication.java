package com.zhihu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.config.server.EnableConfigServer;

/**
 * @author : zhangzheng
 * ClassName: ZhiHuEduConfigApplication
 * @date : 2021/9/2 20:09
 */
@SpringBootApplication
@EnableConfigServer
@EnableDiscoveryClient
public class ZhiHuEduConfigApplication {
    public static void main(String[] args) {
        SpringApplication.run(ZhiHuEduConfigApplication.class);
    }
}
