package com.zhihu.service;

import com.zhihu.pojo.OpusCommNums;

import java.util.List;

public interface OpnRedisService {

    //将已发布的作品类型id和id 拼接成key值
    void savePublishedOpusRedis(String opusTap,String opusId);

    //获取已发布作品的评论数
    Integer getOpusCommNumsFromRedis(String opusTap,String opusId);

    //作品的评论数增加1
    void incrementOpusCommNums(String opusTap,String opusId);

    //作品的评论数减少1
    void decrementOpusCommNums(String opusTap,String opusId);

    //从redis中获取所有作品评论数的数据
    List<OpusCommNums> getCommNumsDataFromRedis();

    //同步redis中的数据更新到数据库mysql中
    void updateCommNumsToMysql();
}
