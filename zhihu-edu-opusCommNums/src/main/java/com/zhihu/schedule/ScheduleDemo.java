package com.zhihu.schedule;

import com.zhihu.service.OpnRedisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class ScheduleDemo {
    @Autowired
    OpnRedisService opnRedisService;
    /**
     * 定时任务方法
     * @Schedule:设置定时任务
     * cron属性：cron表达式，定时任务触发是时间的一个字符串表达形式
     */
    @Scheduled(cron="0 0/5 * * * ?")
    public void scheduledMethod(){
        //将 Redis 里的点赞信息同步到数据库里
        opnRedisService.updateCommNumsToMysql();
    }
}
