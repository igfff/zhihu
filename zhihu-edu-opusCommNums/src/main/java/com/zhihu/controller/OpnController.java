package com.zhihu.controller;

import com.zhihu.pojo.OpusCommNums;
import com.zhihu.service.OpnRedisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RequestMapping("/opn")
@RestController
public class OpnController {

    @Autowired
    OpnRedisService opnRedisService;

    //从redis中获取作品（问题）的评论数
    @RequestMapping(value = "/getOpusCommNumsFromRedis",method = RequestMethod.GET)
    public Integer getOpusCommNumsFromRedis(@RequestParam("opusTap")Integer opusTap, @RequestParam("opusId")Integer opusId){
        return opnRedisService.getOpusCommNumsFromRedis(opusTap.toString(),opusId.toString());
    }

    //Redis中评论增加1
    @RequestMapping(value = "/incrementOpusCommNums",method = RequestMethod.GET)
    public void incrementOpusCommNums(@RequestParam("opusTap")Integer opusTap,@RequestParam("opusId")Integer opusId){
        opnRedisService.incrementOpusCommNums(opusTap.toString(),opusId.toString());
    }

    //Redis中评论减少1
    @RequestMapping(value = "/decrementOpusCommNums",method = RequestMethod.GET)
    public void decrementOpusCommNums(@RequestParam("opusTap")Integer opusTap,@RequestParam("opusId")Integer opusId){
        opnRedisService.decrementOpusCommNums(opusTap.toString(),opusId.toString());
    }

    //将作品类型ID和Id拼接作为key值添加到redis中，并且初始化value评论数为0
    @RequestMapping(value = "/insertKeyToRedis",method = RequestMethod.GET)
    public void insertKeyToRedis(@RequestParam("opusTap")Integer opusTap,@RequestParam("opusId")Integer opusId){
        opnRedisService.savePublishedOpusRedis(opusTap.toString(),opusId.toString());
    }

    //从Redis中获取所有有关评论数的数据，为同步进mysql作前置准备
    @RequestMapping(value = "/getCommNumsDataFromRedis",method = RequestMethod.GET)
    public List<OpusCommNums> getCommNumsDataFromRedis(){
        return opnRedisService.getCommNumsDataFromRedis();
    }
}
