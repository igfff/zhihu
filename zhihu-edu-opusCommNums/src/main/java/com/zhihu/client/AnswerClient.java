package com.zhihu.client;

import com.zhihu.pojo.baseVo.BaseResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "zhihu-edu-answer",path = "/answer")

public interface AnswerClient {

    @RequestMapping(value = "/findAnswerById",method = RequestMethod.POST)
    public BaseResult findAnswerById(@RequestParam("answerId") Integer answerId);
}
