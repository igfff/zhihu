package com.zhihu.client;

import com.zhihu.pojo.baseVo.BaseResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "zhihu-edu-question",path = "/question")
public interface QuestionClient {

    @RequestMapping(value = "/findQuestionById",method = RequestMethod.POST)
    public BaseResult findQuestionById(@RequestParam("questionId") Integer questionId);

}
