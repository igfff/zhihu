package com.zhihu.service;


import com.zhihu.pojo.baseVo.BaseResult;

import javax.servlet.http.HttpServletRequest;

public interface IndexService {
    BaseResult findIndexList(HttpServletRequest request);

    BaseResult findHotList();

    void findHotQuestion();
}
