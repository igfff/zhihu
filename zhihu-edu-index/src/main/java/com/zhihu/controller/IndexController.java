package com.zhihu.controller;

import com.zhihu.pojo.baseVo.BaseResult;
import com.zhihu.service.IndexService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/index")
public class IndexController {

    @Autowired
    IndexService indexService;

    /**
     *获取首页推荐的问答
     * @param request 浏览器请求
     * @return   BaseResult
     */
    @RequestMapping("/findIndexList")
    public BaseResult findIndexList(HttpServletRequest request){
        return indexService.findIndexList(request);
    }

    /**
     * 获取热度排行榜问题
     * @return BaseResult
     */
    @RequestMapping("/findHotList")
    public BaseResult findHotList(){
        return indexService.findHotList();
    }
}
