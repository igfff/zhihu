package com.zhihu.client;

import com.zhihu.pojo.baseVo.BaseResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "zhihu-edu-answer",path = "/answer")
public interface AnswerClient {

    //通过问题id查询该问题所有回答,并按点赞数量从高到低排序
    @RequestMapping(value = "/findAnswersByQuestionId",method = RequestMethod.GET)
    public BaseResult findAnswersByQuestionId(@RequestParam("questionId")Integer questionId);
}
