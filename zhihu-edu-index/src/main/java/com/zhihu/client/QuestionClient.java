package com.zhihu.client;

import com.zhihu.pojo.baseVo.BaseResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "zhihu-edu-Question",path = "/question")
public interface QuestionClient {

    @RequestMapping(value = "/findFavorQuesByUserId",method = RequestMethod.GET)
    public BaseResult findFavorQuesByUserId(@RequestParam("userId")Integer userId);

    //通过标签id查询所有绑定标签的问题
    @RequestMapping(value = "/findQuesByTypeId",method = RequestMethod.GET)
    public BaseResult findQuesByTypeId(@RequestParam("typeId")Integer typeId);

    //查询所有问题
    @RequestMapping(value = "/findAllQuestion",method = RequestMethod.POST)
    public BaseResult findAllQuestion();
}
