package com.zhihu.client;

import com.zhihu.pojo.User;
import com.zhihu.pojo.baseVo.BaseResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@FeignClient(name = "zhihu-edu-user",path = "/user")
public interface UserClient {

    @RequestMapping(value = "/findUserById",method = RequestMethod.GET)
    public BaseResult findUserById(Integer userId);

    @RequestMapping(value = "/findAllUsers",method = RequestMethod.GET)
    public List<User> findAllUsers();
}
