package com.zhihu.dao;

import com.zhihu.pojo.Chat;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ChatRepository extends JpaRepository<Chat,Integer> {

    List<Chat> findByFromUserIdAndFromUserChatStatus(Integer oid,Integer status);

    List<Chat> findByToUserIdAndToUserChatStatus(Integer tid ,Integer status);

    Chat findAllById(Integer id);

    List<Chat> findByFromUserIdAndToUserId(Integer oid,Integer tid);

}
