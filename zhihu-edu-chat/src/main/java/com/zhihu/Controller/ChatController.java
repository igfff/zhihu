package com.zhihu.Controller;

import com.zhihu.pojo.baseVo.BaseResult;
import com.zhihu.service.ChatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/chat")
public class ChatController {

    @Autowired
    ChatService chatService;

    /**
     * 根据用户id查询所有消息的集合
     * @param userId  用户id
     * @return          BaseResult
     */
    @RequestMapping(value = "/findChatByUserId",method = RequestMethod.GET)
    public BaseResult findChatByUserId(@RequestParam("userId")String userId){
        return chatService.findChatByUserId(Integer.valueOf(userId));
    }

    /**
     * 删除当前用户的某条消息
     * @param chatId    消息id
     * @param userId    用户id
     * @return          BaseResult
     */
    @RequestMapping(value = "/deleteByChatId",method = RequestMethod.GET)
    public BaseResult deleteByChatId(@RequestParam("chatId")String chatId,@RequestParam("userId")String userId){
        return chatService.deleteByChatId(Integer.valueOf(chatId),Integer.valueOf(userId));
    }

    /**
     * 删除当前用户与某个用户的对话
     * @param otherUserId    需要对话的用户id
     * @param userId         当前用户id
     * @return               BaseResult
     */
    @RequestMapping(value = "/deleteSomeChat",method = RequestMethod.GET)
    public BaseResult deleteSomeChat(@RequestParam("userId")String otherUserId,@RequestParam("otherUserId")String userId){
        return chatService.deleteSomeChat(Integer.valueOf(userId),Integer.valueOf(otherUserId));
    }

    /**
     * 将当前用户与另一个用户的未读聊天记录更改为已读
     * @param userId        当前用户id
     * @param otherUserId   对话用户id
     * @return              BaseResult
     */
    @RequestMapping(value = "/updateChatStatus",method = RequestMethod.GET)
    public BaseResult updateChatStatus(@RequestParam("userId")String userId,@RequestParam("otherUserId")String otherUserId){
        return chatService.updateChatStatus(Integer.valueOf(userId),Integer.valueOf(otherUserId));
    }

    /**
     * 获取未读消息个数
     * @param userId    当前用户id
     * @return          BaseResult
     */
    @RequestMapping(value = "/findAllUnreadChat",method = RequestMethod.GET)
    public BaseResult findAllUnreadChat(@RequestParam("userId")String userId){
        return chatService.findAllUnreadChat(Integer.valueOf(userId));
    }
}
