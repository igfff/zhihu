package com.zhihu.service;

import com.zhihu.pojo.Chat;
import com.zhihu.pojo.baseVo.BaseResult;

public interface ChatService {
    BaseResult findChatByUserId(Integer userId);

    BaseResult insertChat(Chat chat);

    BaseResult deleteByChatId(Integer chatId,Integer userId);

    BaseResult updateChatStatus(Integer userId,Integer otherUserId);

    BaseResult findAllUnreadChat(Integer userId);

    BaseResult deleteSomeChat(Integer userId, Integer otherUserId);
}
