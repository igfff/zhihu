package com.zhihu.service.Impl;

import com.zhihu.dao.ChatRepository;
import com.zhihu.pojo.Chat;
import com.zhihu.pojo.baseVo.BaseResult;
import com.zhihu.service.ChatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class ChatServiceImpl implements ChatService {

    @Autowired
    ChatRepository chatRepository;

    @Override
    public BaseResult findChatByUserId(Integer id) {
        List<Chat> byFromUserId = chatRepository.findByFromUserIdAndFromUserChatStatus(id,1);
        List<Chat> toFromUserId = chatRepository.findByToUserIdAndToUserChatStatus(id,2);
        byFromUserId.addAll(toFromUserId);
        if (byFromUserId.isEmpty()){
            return new BaseResult(0,"未查到聊天记录");
        }
        Set<Integer> userIds = new HashSet<>();
        for (Chat chat : byFromUserId) {
            userIds.add(chat.getFromUserId());
            userIds.add(chat.getToUserId());
            userIds.remove(id);
        }
        Map<Integer, List<Chat>> chatMap = new HashMap<>();
        for (Integer userId : userIds) {
            List<Chat> chatList = new ArrayList<>();
            for (Chat chat : byFromUserId) {
                if (chat.getFromUserId().equals(userId) || chat.getToUserId().equals(userId)){
                    chatList.add(chat);
                }
            }
            chatMap.put(userId,chatList);
        }
        return new BaseResult(1,"查询成功",chatMap);

    }

    @Override
    public BaseResult insertChat(Chat chat) {

        chatRepository.saveAndFlush(chat);
        return new BaseResult(1,"添加成功");

    }

    @Override
    public BaseResult deleteByChatId(Integer chatId, Integer userId) {
        Chat allById = chatRepository.findAllById(chatId);
        if (allById.getFromUserId().equals(userId)){
            allById.setFromUserChatStatus(0);
            chatRepository.saveAndFlush(allById);
        }else {
            allById.setToUserChatStatus(0);
            chatRepository.saveAndFlush(allById);
        }

        return new BaseResult(1,"修改成功");
    }

    @Override
    public BaseResult updateChatStatus(Integer userId,Integer otherUserId) {
        List<Chat> list = chatRepository.findByFromUserIdAndToUserId(otherUserId, userId);
        if (!list.isEmpty()){
            for (Chat chat : list) {
                if (chat.getToUserChatStatus()!=0){
                    chat.setToUserChatStatus(2);
                    chatRepository.saveAndFlush(chat);
                }
            }
            return new BaseResult(1,"修改成功");
        }

        return new BaseResult(0,"未查询到未读消息");
    }

    @Override
    public BaseResult findAllUnreadChat(Integer userId) {
        List<Chat> list = chatRepository.findByToUserIdAndToUserChatStatus(userId, 1);
        int size = list.size();
        if (size>0){
            return new BaseResult(1,"查询成功",(long)size,null);
        }else {
            return new BaseResult(0,"未查到符合条件的信息");
        }
    }

    @Override
    public BaseResult deleteSomeChat(Integer userId, Integer otherUserId) {
        List<Chat> list = chatRepository.findByFromUserIdAndToUserId(otherUserId, userId);
        if (!list.isEmpty()){
            for (Chat chat : list) {
                chat.setToUserChatStatus(0);
                chatRepository.saveAndFlush(chat);
            }
        }
        List<Chat> list2 = chatRepository.findByFromUserIdAndToUserId(userId,otherUserId);
        if (!list.isEmpty()){
            for (Chat chat : list2) {
                chat.setFromUserChatStatus(0);
                chatRepository.saveAndFlush(chat);
            }
        }
        return new BaseResult(1,"删除成功");
    }

}
