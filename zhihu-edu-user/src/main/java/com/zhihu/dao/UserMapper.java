package com.zhihu.dao;

import com.zhihu.pojo.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author : zhangzheng
 * ClassName: UserMapper
 * @date : 2021/9/6 20:43
 */

@Mapper
public interface UserMapper {
   User findByTel(String telephone);

   List<User> findAllUser();

   User findUserById(@Param("userId") Integer userId);
}
