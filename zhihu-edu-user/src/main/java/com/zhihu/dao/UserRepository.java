package com.zhihu.dao;

import com.zhihu.pojo.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author : zhangzheng
 * ClassName: UserRepository
 * @date : 2021/9/3 15:18
 */
public interface UserRepository extends JpaRepository<User,Integer> {
    User findByTelephone(String telephone);
}
