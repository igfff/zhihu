package com.zhihu.client;

import com.zhihu.pojo.baseVo.BaseResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author : zhangzheng
 * ClassName: MessageClient
 * @date : 2021/9/3 15:25
 */
@FeignClient(name = "zhihu-edu-message",path = "/message")
public interface MessageClient {
    @RequestMapping(value = "/sendMsg",method = RequestMethod.POST)
    public BaseResult sendMsg(@RequestParam("telephone")String telephone);


}
