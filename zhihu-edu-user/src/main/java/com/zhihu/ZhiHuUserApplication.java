package com.zhihu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @author : zhangzheng
 * ClassName: ZhiHuUserApplication
 * @date : 2021/9/3 11:46
 */
@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients
public class ZhiHuUserApplication {
    public static void main(String[] args) {
        SpringApplication.run(ZhiHuUserApplication.class);
    }
}
