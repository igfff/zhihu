package com.zhihu.service;

import com.zhihu.pojo.User;
import com.zhihu.pojo.baseVo.BaseResult;
import com.zhihu.pojo.req.TbUserReq;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author : zhangzheng
 * ClassName: UserService
 * @date : 2021/9/3 15:23
 */
public interface UserService {
    BaseResult decideTel(String telephone);

    BaseResult sendMsg(String telephone);

    BaseResult registry(TbUserReq tbUserReq);

    BaseResult login(TbUserReq tbUserReq);

   // BaseResult upload(MultipartFile multipartFile);

    BaseResult loginMsg(TbUserReq tbUserReq);

    BaseResult insertConsumer(@RequestBody TbUserReq tbUserReq);

    BaseResult updateInformation(TbUserReq tbUserReq, HttpServletRequest request);

    BaseResult loginOut(HttpServletRequest request);

    BaseResult findUser(HttpServletRequest request);

    List<User> findAllUsers();

    BaseResult findUserById(Integer userId);

    BaseResult updateUser(TbUserReq tbUserReq,HttpServletRequest request);
}
