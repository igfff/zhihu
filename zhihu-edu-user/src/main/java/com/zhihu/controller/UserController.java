package com.zhihu.controller;

import com.zhihu.pojo.User;
import com.zhihu.pojo.baseVo.BaseResult;
import com.zhihu.pojo.req.TbUserReq;
import com.zhihu.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author : zhangzheng
 * ClassName: UserController
 * @date : 2021/9/3 15:22
 */
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    UserService userService;

    /**
     * 验证用户
     * @param telephone
     * @return
     */
     @RequestMapping(value = "/decideTel",method = RequestMethod.GET)
     public BaseResult decideTel (@RequestParam String telephone){
         return userService.decideTel(telephone);
     }

    /**
     * 短信验证发送
     * @param telephone
     * @return
     */
    @RequestMapping(value = "/getCode",method = RequestMethod.GET)
    public BaseResult getCode(@RequestParam("telephone") String telephone){
        return userService.sendMsg(telephone);
    }

    /**
     * 用户注册
     * @param tbUserReq
     * @return
     */
    @RequestMapping(value = "/registry",method = RequestMethod.POST)
    public BaseResult registry(@RequestBody TbUserReq tbUserReq){
        return userService.registry(tbUserReq);
    }

    /**
     * 用户登录密码
     * @param tbUserReq
     * @return
     */
    @RequestMapping(value = "/login",method = RequestMethod.POST)
    public BaseResult login(@RequestBody TbUserReq tbUserReq){
        return userService.login(tbUserReq);
    }

    /**
     * 短信验证登录
     * @param tbUserReq
     * @return
     */
    @RequestMapping(value = "/loginMsg",method = RequestMethod.POST)
    public BaseResult loginMsg(@RequestBody TbUserReq tbUserReq){
        return userService.loginMsg(tbUserReq);
    }

    /**
     *用户退出登录
     * @param request
     * @return
     */
    @RequestMapping(value = "/loginOut",method = RequestMethod.POST)
    public BaseResult loginOut(HttpServletRequest request){
        return userService.loginOut(request);
    }

    /**
     * 用户密码信息添加
     * @param tbUserReq
     * @return
     */
    @RequestMapping(value = "/insertConsumer",method = RequestMethod.POST)
    public BaseResult insertConsumer(@RequestBody TbUserReq tbUserReq){
        return userService.insertConsumer(tbUserReq);
    }

    /**
     * 个人资料修改
     * @param tbUserReq
     * @param request
     * @return
     */
    @RequestMapping(value = "/updateInformation",method = RequestMethod.POST)
    public BaseResult updateInformation (@RequestBody TbUserReq tbUserReq,HttpServletRequest request){
        return userService.updateInformation(tbUserReq,request);
    }

    /**
     * 登录用户信息查看
     * @param request
     * @return
     */
    @RequestMapping(value = "/findUserByToken",method = RequestMethod.POST)
    public BaseResult findUserByToken(HttpServletRequest request){
        return userService.findUser(request);
    }

    /**
     * 查询所有用户得信息
     * @return
     */
    @RequestMapping(value = "/findAllUsers",method = RequestMethod.GET)
    public List<User> findAllUsers(){
        return userService.findAllUsers();
    }

    /**
     * 根据用户id查询用户的信息
     * @param userId
     * @return
     */
    @RequestMapping(value = "/findUserById",method = RequestMethod.GET)
    public BaseResult findUserById(Integer userId){
        return userService.findUserById(userId);
    }

    /**
     * 修改
     * @param tbUserReq
     * @param request
     * @return
     */
    @RequestMapping(value = "/updateUser",method = RequestMethod.POST)
    public BaseResult updateUser(@RequestBody TbUserReq tbUserReq,HttpServletRequest request){
        return userService.updateUser(tbUserReq,request);
    }
//    /**
//     * 图片上传
//     * @param multipartFile
//     * @return
//     */
//    @RequestMapping("/upload")
//    public BaseResult uploadImg(@RequestParam("file") MultipartFile multipartFile){
//        return userService.upload(multipartFile);
//    }
}