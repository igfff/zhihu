package com.zhihu.pojo;

import lombok.Data;

@Data
public class Column {

    //专栏id
    private Integer columnId;

    //专栏标题
    private String colTitle;

    //专栏介绍
    private String colInfo;

    //用户id
    private Integer userId;
}
