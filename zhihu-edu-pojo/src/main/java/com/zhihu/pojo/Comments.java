package com.zhihu.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.persistence.*;
import javax.persistence.Column;
import java.util.Date;

@Data
@Entity
@Table(name = "zhihu_comments")
public class Comments {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "comments_id")
    //评论的id
    private Integer commentsId;

    @Column(name = "user_id")
    //用户的id
    private Integer userId;

    @Column(name = "comm_username")
    //当前评论的用户名
    private String commUserName;

    @Column(name = "comm_userimg")
    //当前评论的用户头像
    private String commUserImg;

    @Column(name = "comm_content")
    //评论的内容
    private String commContent;

    @Column(name = "follow_user_id")
    //被评论用户的id
    private  Integer followUserId;

    @Column(name = "follow_username")
    //被评论的用户名
    private String followUserName;

    @Column(name = "follow_userimg")
    //被评论的用户头像
    private  String followUserImg;

    @JsonFormat(pattern = "yyyy-MM-dd")
    @Column(name = "comm_createtime")
    //评论创建的时间
    private Date commCreatetime;

    @Column(name = "comm_likenum")
    //评论的点赞数
    private Integer commLikenum;

    @Column(name = "comm_status")
    //评论的状态（0：删除，1：未删除）
    private Integer commStatus;

    @Column(name = "follow_opus_tap")
    //被评论作品的类型
    private Integer followOpusTap;

    @Column(name = "follow_opus_id")
    //被评论作品的id（可以是评论Id）
    private Integer followOpusId;
}
