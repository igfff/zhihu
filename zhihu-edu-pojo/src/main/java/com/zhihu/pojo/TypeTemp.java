package com.zhihu.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.persistence.Column;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "zhihu_type_temp")
public class TypeTemp {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "type_temp_id")
    private Integer typeTempId;

    //作品id
    @Column(name = "tt_opus")
    private Integer ttOpus;

    //作品类型id
    @Column(name = "tt_opus_tap")
    private Integer ttOpusTap;

    @Column(name = "type_id")
    private Integer typeId;
}
