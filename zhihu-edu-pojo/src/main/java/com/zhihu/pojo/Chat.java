package com.zhihu.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "zhihu_chat")
public class Chat {

    //消息id
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "from_userimg")
    //发信人头像
    private String fromUserImg;

    @Column(name = "from_username")
    //发信人名字
    private String fromUserName;

    @Column(name = "from_userid")
    //发信人id
    private Integer fromUserId;

    @Column(name = "chat_text")
    //消息内容
    private String chatText;

    @Column(name = "chat_time")
    //消息发送时间
    private String chatTime;

    @Column(name = "to_userimg")
    //收信人头像
    private String toUserImg;

    @Column(name = "to_username")
    //收信人名字
    private String toUserName;

    @Column(name = "to_userid")
    //收信人id
    private Integer toUserId;

    @Column(name = "from_status")
    //发信人信息状态
    private Integer fromUserChatStatus;

    @Column(name = "to_status")
    //收信人信息状态
    private Integer toUserChatStatus;

}
