package com.zhihu.pojo;

import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class Article {

    private Integer articleId;

    //文章主题图片
    private String ansTitleImg;

    //文章的标题
    private String artTitle;

    //文章的内容
    private String artContent;

    //文章的创建时间
    private Date artCreatetime;

    //文章的点赞数
    private Integer artLikenum;

    //用户id
    private Integer userId;

    //是否存在于专栏(0:不存在；1:存在)
    private Integer column;

    //作品类型：文章的类型指定为4
    private Integer opusTap;

    //文章的评论总数
    private Integer artCommNums;

    //文章的作者名字
    private String artUserName;

    //文章的作者头像
    private String userImg;

}
