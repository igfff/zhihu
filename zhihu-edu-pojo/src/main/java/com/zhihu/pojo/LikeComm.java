package com.zhihu.pojo;

import com.zhihu.Enums.LikedStatusEnum;
import lombok.Data;

@Data
public class LikeComm {

    //id
    private Integer lcId;

    //被点赞的用户的id
    private String likedUserId;

    //点赞的用户的id
    private String likedPostId;

    //被评论作品的id
    private String lcCommentId;

    //评论的点赞状态,默认是未点赞状态
    private Integer lcStatus = LikedStatusEnum.UNLIKE.getCode();

    public LikeComm(String likedUserId, String likedPostId,String lcCommentId, Integer lcStatus){
        this.likedUserId = likedUserId;
        this.likedPostId = likedPostId;
        this.lcCommentId = lcCommentId;
        this.lcStatus = lcStatus;
    }

    public LikeComm(){

    }
}
