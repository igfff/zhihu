package com.zhihu.pojo;

import lombok.Data;

import javax.persistence.*;
import javax.persistence.Column;

@Data
@Entity
@Table(name = "zhihu_user")
public class User {
    //用户id
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id")
    private Integer userId;
    //用户昵称
    @Column(name = "user_name")
    private String userName;
    //用户密码
    @Column(name = "password")
    private String password;
    //用户账号
    @Column(name = "telephone")
    private String telephone;
    //用户
    @Column(name = "email")
    private String email;
    //用户的性别
    @Column(name = "sex")
    private String sex;
    //用户的头像

    private String img;
    //资料背景图
    private String bgImg;
    //个人签名
    @Column(name = "intro")
    private String intro;
    //地址
    @Column(name = "address")
    private String address;
    //行业
    @Column(name = "job")
    private String job;
    //个人职业经历
    @Column(name = "job_exp")
    private String jobExp;
    //个人简介
    @Column(name = "info")
    private String info;
}
