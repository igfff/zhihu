package com.zhihu.pojo;

/**
 * 点赞数量 DTO。用于存储从 Redis 取出来的被点赞数量
 */
import lombok.Data;

@Data
public class LikedOpusCount {

    private String id;

    //点赞作品的id
    private String followOpusId;

    //点赞作品的类型
    private String followOpusTap;

    private Integer count;

    public LikedOpusCount() {
    }

    public LikedOpusCount(String followOpusId,String followOpusTap, Integer count) {
        this.followOpusId = followOpusId;
        this.followOpusTap = followOpusTap;
        this.count = count;
    }
}
