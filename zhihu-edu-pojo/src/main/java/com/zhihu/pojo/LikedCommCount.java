package com.zhihu.pojo;

/**
 * 点赞数量 DTO。用于存储从 Redis 取出来的被点赞数量
 */
import lombok.Data;

@Data
public class LikedCommCount {

    private String id;

    //点赞评论的id
    private String commentsId;

    //该评论的总点赞数
    private Integer count;

    public LikedCommCount() {
    }

    public LikedCommCount(String commentsId, Integer count) {
        this.commentsId = commentsId;
        this.count = count;
    }
}
