package com.zhihu.pojo.baseVo;

import lombok.Data;

/**
 * @author : zhangzheng
 * ClassName: BaseResult
 * @date : 2021/9/2 20:25
 */
@Data
public class BaseResult {
        private Integer code;
        private String msg;
        private Long count;
        private Object data;

        public BaseResult(Integer code, String msg) {
            this.code = code;
            this.msg = msg;
        }
        public BaseResult(Integer code, String msg, Object data) {
            this.code = code;
            this.msg = msg;
            this.data = data;
        }

        public BaseResult(Integer code, String msg, Long count, Object data) {
            this.code = code;
            this.msg = msg;
            this.count = count;
            this.data = data;
        }

    public BaseResult() {
    }
}
