package com.zhihu.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.persistence.Column;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "zhihu_question")
public class Question {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "question_id")
    private Integer questionId;

    @Column(name = "ques_title")
    //问题的标题
    private String quesTitle;

    @Column(name = "ques_info")
    //问题简介
    private String quesInfo;

    @Column(name = "opus_tap")
    //作品类型：提问的类型指定为1
    private Integer opusTap;

    @Column(name = "ques_img")
    //问题内容附加的图片
    private String quesImg;

    @Column(name = "ques_vio")
    //问题内容附加的视频
    private String quesVio;

    @Column(name = "ques_comm_nums")
    //当前问题的评论总数
    private Integer quesCommNums;

    @Column(name = "user_id")
    //用户id
    private Integer userId;

    @Column(name = "ques_username")
    //当前作者的用户名
    private String quesUserName;

    @Column(name = "ques_userimg")
    //当前作者的用户头像
    private String quesUserImg;

    @JsonFormat(pattern = "yyyy-MM-dd")
    @Column(name = "ques_createtime")
    //问题创建的时间
    private Date quesCreatetime;

    @Column(name = "ques_likenum")
    //问题的点赞数
    private Integer quesLikeNum;
}
