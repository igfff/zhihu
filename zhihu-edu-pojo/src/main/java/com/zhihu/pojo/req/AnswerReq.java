package com.zhihu.pojo.req;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.zhihu.pojo.Type;
import lombok.Data;

import java.util.Date;

@Data
public class AnswerReq {

    private Integer answerId;

    //回答的标题
    private String ansTitle;

    //回答的内容
    private String ansContent;


    @JsonFormat(pattern = "yyyy-MM-dd")
    //回答的时间
    private Date ansCreatetime;

    //点赞数
    private Integer ansLikenum;

    //用户id
    private Integer userId;

    //回答内容附加的图片
    private String ansImg;

    //回答内容附加的视频
    private String ansVio;

    //是否存在于专栏(0:不存在；1:存在)
    private Integer column;

    //作品类型：回答的类型指定为2
    private Integer opusTap;

    //问题的id
    private Integer questionId;

    //回答的评论总数
    private Integer ansCommNums;

    //当前回答的用户名字
    private String ansUserName;

    //当前回答的用户头像
    private String ansUserImg;

}
