package com.zhihu.pojo.req;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.zhihu.pojo.Comments;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class CommentsReq {

    private Integer commentsId;

    private Integer userId;

    private String commUserName;

    private String commUserImg;

    private String commContent;

    private  Integer followUserId;

    private String followUserName;

    private String followUserImg;

    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date commCreatetime;

    private Integer commLikenum;

    private Integer commStatus;

    private Integer followOpusTap;

    private Integer followOpusId;

    private List<Comments> commentsList;
}
