package com.zhihu.pojo.req;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.solr.client.solrj.beans.Field;
import org.springframework.data.annotation.Id;
import org.springframework.data.solr.core.mapping.SolrDocument;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@SolrDocument(solrCoreName="demo3")
public class TypeReq implements Serializable {

    @Id
    @Field("id")
    private String typeId;

    @Field("type_name")
    private String typeName;
}
