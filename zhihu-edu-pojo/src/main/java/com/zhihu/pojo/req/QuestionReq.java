package com.zhihu.pojo.req;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.zhihu.pojo.Type;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class QuestionReq {

    private Integer questionId;

    //问题的标题
    private String quesTitle;

    //问题简介
    private String quesInfo;

    //作品类型：提问的类型指定为1
    private Integer opusTap;

    //问题内容附加的图片
    private String quesImg;

    //问题内容附加的视频
    private String quesVio;

    //当前问题的评论总数
    private Integer quesCommNums;

    //用户id
    private Integer userId;

    //当前作者的用户名
    private String quesUserName;

    //当前作者的用户头像
    private String quesUserImg;

    @JsonFormat(pattern = "yyyy-MM-dd")
    //问题创建的时间
    private Date quesCreatetime;

    //问题的点赞数
    private Integer quesLikeNum;

    //标签的集合
    private List<Type> typesList;
}
