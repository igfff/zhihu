package com.zhihu.pojo.req;

import lombok.Data;
import org.apache.solr.client.solrj.beans.Field;
import org.springframework.data.annotation.Id;
import org.springframework.data.solr.core.mapping.SolrDocument;

import java.io.Serializable;
import java.util.Date;


@Data
@SolrDocument(solrCoreName="demo1")
public class SolrSearchQues implements Serializable {

    @Id
    @Field("id")
    private String questionId;

    @Field("ques_title")
    private String quesTitle;

    @Field("ques_info")
    private String quesInfo;

    @Field("opus_tap")
    private Integer opusTap;

    @Field("ques_img")
    private String quesImg;

    @Field("ques_vio")
    private String quesVio;

    @Field("ques_comm_nums")
    private Long quesCommNums;

    @Field("user_id")
    private Integer userId;

    @Field("ques_username")
    private String quesUsername;

    @Field("ques_userimg")
    private String quesUserimg;

    @Field("ques_createtime")
    private Date quesCreatetime;

    @Field("ques_likenum")
    private Long quesLikenum;
}
