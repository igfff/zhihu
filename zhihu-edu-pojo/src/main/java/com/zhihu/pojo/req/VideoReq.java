package com.zhihu.pojo.req;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.zhihu.Enums.AutherAndForward;
import com.zhihu.pojo.Type;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.List;

@Data
public class VideoReq {

    private Integer videoId;

    //视频的标题
    private String viTitle;

    //视频路径
    private String viUrl;

    //二层key
    private String videoPath;

    //视频封面图片
    private String viImg;

    //视频背景图片的数组
    private  String[] viImages;

    //视频大小
    private String viSize;

    //视频时长
    @JsonFormat(pattern = "HH:mm:ss" )
    /*@DateTimeFormat(pattern = "HH:mm:ss" )*/
    private Date viLength;

    @JsonFormat( pattern = "yyyy-MM-dd",  timezone = "GMT+8" )
    /*@DateTimeFormat(pattern = "yyyy-MM-dd" )*/
    //回答的时间
    private Date viCreatetime;

    //视频简介
    private String viInfo;

    //播放次数，这个写死就行
    private Integer viPlaynum;

    //点赞数
    private Integer viLikenum;

    //用户id
    private Integer userId;

    //是否存在于专栏(0:不存在；1:存在)
    private String column;

    //作品类型：视频的类型指定为3
    private Integer opusTap;

    //当前视频的评论总数
    private Integer viCommNums;

    //当前视频作者的名字
    private String viUserName;

    //当前视频作者的头像
    private String viUserImg;

    //上传视频状态
    private Integer status;

    //标签的集合
    private List<Type> typesList;

    //若用户发视频添加问题
    private String quesTitle;

    //若用户发视频添加问题的描述
    private String quesInfo;

    //是否原创
    private Integer autherAndForward;
}
