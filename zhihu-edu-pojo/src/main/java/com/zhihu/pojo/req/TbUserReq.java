package com.zhihu.pojo.req;

import lombok.Data;

import javax.persistence.Column;

/**
 * @author : zhangzheng
 * ClassName: TbUserReq
 * @date : 2021/9/9 11:11
 */
@Data
public class TbUserReq {
    private Integer userId;
    //用户昵称
    private String userName;
    //用户密码
    private String password;
    //用户账号
    private String telephone;
    //用户
    private String email;
    //用户的性别
    @Column(name = "sex")
    private String sex;
    //用户的头像
    private String img;
    //资料背景图
    private String bgImg;
    //个人签
    private String intro;
    //地址
    private String address;
    //行业
    private String job;
    //个人职业经历
    private String jobExp;
    //个人简介
    private String info;
}
