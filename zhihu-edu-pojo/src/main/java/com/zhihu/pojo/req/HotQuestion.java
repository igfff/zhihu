package com.zhihu.pojo.req;

import lombok.Data;

@Data
public class HotQuestion {

    //问题的id
    private Integer questionId;

    //问题的标题
    private String quesTitle;

    //问题简介
    private String quesInfo;

    //作品类型：提问的类型指定为1
    private Integer opusTap;

    //问题内容附加的图片
    private String quesImg;

    //问题的热度
    private Integer hotAmount;
}
