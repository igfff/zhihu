package com.zhihu.pojo;

import com.zhihu.Enums.LikedStatusEnum;
import lombok.Data;

import javax.persistence.*;
import javax.persistence.Column;

@Data
@Entity
@Table(name = "zhihu_like_opus")
public class LikeOpus {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "like_id")
    private Integer likeId;

    //被点赞的用户的id
    @Column(name = "like_user_id")
    private String likedUserId;

    //点赞的用户的id
    @Column(name = "like_post_id")
    private String likedPostId;

    //点赞作品或评论的id
    @Column(name = "follow_opus_id")
    private String followOpusId;

    //点赞作品或评论的类型
    @Column(name = "follow_opus_tap")
    private String followOpusTap;

    //作品的点赞状态,默认未点赞状态
    @Column(name = "like_status")
    private Integer likeStatus = LikedStatusEnum.UNLIKE.getCode();

    public LikeOpus(String likedUserId,String likedPostId,String followOpusId,String followOpusTap,Integer likeStatus) {
        this.likedUserId = likedUserId;
        this.likedPostId = likedPostId;
        this.followOpusId = followOpusId;
        this.followOpusTap = followOpusTap;
        this.likeStatus = likeStatus;
    }

    public LikeOpus(){

    }
}
