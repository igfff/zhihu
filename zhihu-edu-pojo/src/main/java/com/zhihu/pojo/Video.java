package com.zhihu.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.persistence.Column;
import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "zhihu_video")
public class Video {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "video_id")
    private Integer videoId;

    //视频的标题
    @Column(name = "vi_title")
    private String viTitle;

    //视频路径
    @Column(name = "vi_url")
    private String viUrl;

    //视频封面图片
    @Column(name = "vi_img")
    private String viImg;

    //视频大小
    @Column(name = "vi_size")
    private String viSize;

    //视频时长
    @Column(name = "vi_length")
    @JsonFormat( pattern = "HH:mm:ss",  timezone = "GMT+8" )
    private Date viLength;

    //回答的时间
    @Column(name = "vi_createtime")
    @JsonFormat( pattern = "yyyy-MM-dd",  timezone = "GMT+8" )
    private Date viCreatetime;

    //视频简介
    @Column(name = "vi_info")
    private String viInfo;

    //播放次数，这个写死就行
    @Column(name = "vi_playnum")
    private Integer viPlaynum;

    //点赞数
    @Column(name = "vi_likenum")
    private Integer viLikenum;

    //用户id
    @Column(name = "user_id")
    private Integer userId;

    //是否存在于专栏(0:不存在；1:存在)
    @Column(name = "user_column")
    private String column;

    //作品类型：视频的类型指定为3
    @Column(name = "opus_tap")
    private Integer opusTap;

    //当前视频的评论总数
    @Column(name = "vi_comm_nums")
    private Integer viCommNums;

    //当前视频作者的名字
    @Column(name = "vi_username")
    private String viUserName;

    //当前视频作者的头像
    @Column(name = "vi_userimg")
    private String viUserImg;

    //上传视频状态
    @Column(name = "vi_status")
    private Integer status;
}
