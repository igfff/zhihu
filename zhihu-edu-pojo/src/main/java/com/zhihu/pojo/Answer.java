package com.zhihu.pojo;


import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.persistence.*;
import javax.persistence.Column;
import java.util.Date;

@Data
@Entity
@Table(name = "zhihu_answer")
public class Answer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "answer_id")
    private Integer answerId;

    @Column(name = "ans_title")
    //回答的标题
    private String ansTitle;

    @Column(name = "ans_content")
    //回答的内容
    private String ansContent;

    @JsonFormat(pattern = "yyyy-MM-dd")
    @Column(name = "ans_createtime")
    //回答的时间
    private Date ansCreatetime;

    @Column(name = "ans_likenum")
    //点赞数
    private Integer ansLikenum;

    @Column(name = "user_id")
    //用户id
    private Integer userId;

    @Column(name = "ans_img")
    //回答内容附加的图片
    private String ansImg;

    @Column(name = "ans_vio")
    //回答内容附加的视频
    private String ansVio;

    @Column(name = "user_column")
    //是否存在于专栏(0:不存在；1:存在)
    private Integer column;

    @Column(name = "opus_tap")
    //作品类型：回答的类型指定为2
    private Integer opusTap;

    @Column(name = "question_id")
    //问题的id
    private Integer questionId;

    @Column(name = "ans_comm_nums")
    //回答的评论总数
    private Integer ansCommNums;

    @Column(name = "ans_username")
    //当前回答的用户名字
    private String ansUserName;

    @Column(name = "ans_userimg")
    //当前回答的用户头像
    private String ansUserImg;
}
