package com.zhihu.pojo;

import lombok.Data;


//这个实体是给redis服务的，专门存作品类型和作品id和作品评论数的实体
@Data
public class OpusCommNums {

    private String opusTap;

    private String opusId;

    private Integer commNums;

    public OpusCommNums() {
    }

    public OpusCommNums(String opusTap, String opusId, Integer commNums) {
        this.opusTap = opusTap;
        this.opusId = opusId;
        this.commNums = commNums;
    }
}
