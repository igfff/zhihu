package com.zhihu.pojo;

import lombok.Data;

@Data
public class Type {
    private Integer typeId;

    private String typeName;
}
