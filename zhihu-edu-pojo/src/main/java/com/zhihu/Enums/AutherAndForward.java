package com.zhihu.Enums;

import lombok.Getter;

//作品是否原创
@Getter
public enum AutherAndForward {

        Auther(2,"原创"),
        Forward(3,"转发"),
    ;
        private Integer code;
        private String msg;

    AutherAndForward(Integer code, String msg){
        this.code=code;
        this.msg=msg;
    }
}
