package com.zhihu.dao;

import com.zhihu.pojo.TypeTemp;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TypeTempRepository extends JpaRepository<TypeTemp,Integer> {

}
