package com.zhihu.dao;

import com.zhihu.pojo.LikeOpus;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LikeOpusRepository extends JpaRepository<LikeOpus,Integer> {
}
