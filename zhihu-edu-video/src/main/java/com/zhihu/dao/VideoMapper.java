package com.zhihu.dao;

import com.zhihu.pojo.Type;
import com.zhihu.pojo.req.VideoReq;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface VideoMapper {

    /**
     *将用户看过的视频放入浏览记录
     * @param videoId 视频id
     */
    List<Type> findVideoType(@Param("videoId") Integer videoId);

    /**
     * 根据用户看过的标签id查询相关视频
     * @param typeId 标签id
     * @return  视频集
     */
    List<VideoReq> findTypeVideos(@Param("typeId")Integer typeId);

    /**
     * 展示用户发的视频
     * @return  视频结果集
     */
    List<VideoReq> findVideoByUserId(@Param("userId")Integer userId);
}
