package com.zhihu.service;

import com.zhihu.pojo.LikeOpus;
import com.zhihu.pojo.User;
import com.zhihu.pojo.baseVo.BaseResult;
import org.springframework.web.multipart.MultipartFile;

import java.util.Date;
import java.util.List;


public interface VideoService {

    /**
     * 上传视频，获取视频的时长、大小以及视频的准备插图，存入redis，等待进一步操作
     * @param multipartFile,user 上传视频,用户信息
     * @return baseResult   返回上传结果
     */
    BaseResult upLoadVideo(MultipartFile multipartFile, User user);

    /**
     * 根据用户的登录信息，去草稿箱（redis）拿到上传的视频集合信息
     * @return 用户的上传的视频集合信息
     */
    BaseResult findVideosInRedis(User user);

    /**
     * 根据用户的登录信息，去草稿箱（redis）删除上传的视频集合信息
     * @param videoPath  删除信息结果集
     * @return 删除信息结果集
     */
    BaseResult delVideoInRedis(User user, String videoPath);

    /**
     * redis中查找视频的详细信息
     * @param videoPath 上传的视频路径
     * @param user 用户的登录信息
     * @return 该视频的信息
     */
    BaseResult findOneInRedis(User user, String videoPath);

    /**
     * 上传图片作为封面
     * @param multipartFile 封面照片文件
     * @param videoPath 对应上传的视频的路径
     * @return 上传成功后的回显，以及结果集
     */
    BaseResult upLoadVideoImg(User user, String videoPath, MultipartFile multipartFile);

    /**
     * 选择现成帧图作为封面
     * @param videoImg 封面照片帧图
     * @param videoPath 对应上传的视频的路径
     * @return 上传成功后的回显，以及结果集
     */
    BaseResult chooseVideoImg(User user, String videoImg, String videoPath);

    /**
     * 使用solr实现问题的搜索展示
     * @param key    问题关键字
     * @param page 当前页数
     * @param size   分页
     * @return 包含关键字的结果集
     */
    BaseResult findBySolrSearchQues(String key, Integer page, Integer size);

    /**
     * 选择问题
     * @param questionId solr里的问题id
     * @return 封装好的结果集
     */
    BaseResult chooseSearchQuestion( Integer questionId);

    /**
     * 将问题和描述插入草稿箱
     * @param videoPath redis里的二级key
     * @String quesTitle   问题的标题
     * @param quesInfo 问题的描述
     * @return 封装好的结果集
     */
    BaseResult insertSearchQuestion(User user, String quesTitle, String quesInfo, String videoPath);

    /**
     * 使用solr实现话题的标签展示
     * @param key   标签关键字
     * @param page  当前页数
     * @param size  分页
     * @return
     */
    BaseResult findTypeByTypeReq(String key, Integer page, Integer size);

    /**
     * 绑定话题——添加视频和问题的标签
     * @param videoPath redis里的二级key
     * @param typeId 插入的标签的id
     * @return 封装好的结果集
     */
    BaseResult insertSearchType(User user, String videoPath, Integer typeId);

    /**
     * 查看选择好的视频的标签
     * @param videoPath redis里的二级key
     * @return 封装好的结果集
     */
    BaseResult findTypeByRedis(User user, String videoPath);

    /**
     * 删除选择好的视频的标签
     * @param videoPath redis里的二级key
     * @return 封装好的结果集
     */
    BaseResult delTypeByRedis(User user, String videoPath,Integer typeId);

    /**
     * 修改标题
     * @param videoPath redis里的二级key
     * @param viTitle 修改后的标题
     * @return 封装好的结果集
     */
    BaseResult updateNewTitle(User user, String videoPath, String viTitle);

    /**
     * 修改标题
     * @param videoPath redis里的二级key
     * @param viInfo 修改后的内容详情
     * @return 封装好的结果集
     */
    BaseResult updateNewInfo(User user, String videoPath, String viInfo);

    /**
     * 修改标题
     * @param videoPath redis里的二级key
     * @param autherAndForward 原创还是转发
     * @return 封装好的结果集
     */
    BaseResult updateAutherAndForward(User user, String videoPath, Integer autherAndForward);

    /**
     * 提交表单，插入数据库
     * @param viCreatetime
     * @param videoPath
     * @return
     */
    BaseResult insertVideoToDataBase(User user, String videoPath, Date viCreatetime);

    /**
     *将用户看过的视频放入浏览记录
     * @param videoId 视频id
     */
    BaseResult recordViewHistory(User user, Integer videoId);

    /**
     * 根据默认用户的浏览记录从而向用户推荐视频
     * @return 推荐的视频结果集
     */
    BaseResult recommendVideos();

    /**
     * 根据用户的浏览记录和喜好从而向用户推荐视频
     * @return 推荐的视频结果集
     */
    BaseResult recommendVideos(User user);

    /**
     * 展示具体的某一个视频
     * @param videoId 视频id
     * @return  视频详情结果集
     */
    BaseResult findVideoByVideoId(Integer videoId);

    /**
     * 展示具体的某一个视频
     * @param videoId 视频id
     * @return  视频详情结果集
     */
    BaseResult findVideoByVideoId(User user, Integer videoId);

    /**
     * 展示用户发的视频
     * @return  视频结果集
     */
    BaseResult findVideoByUserId(User user);
}
