package com.zhihu.controller;

import com.alibaba.fastjson.JSONObject;
import com.zhihu.pojo.User;
import com.zhihu.pojo.baseVo.BaseResult;
import com.zhihu.service.RedisService;
import com.zhihu.service.VideoService;
import com.zhihu.utils.JwtUtils;
import com.zhihu.utils.RedisUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Date;

@RequestMapping("/video")
@RestController
public class VideoController {

    private static final RedisUtils redisUtils = new RedisUtils();

    @Autowired
    VideoService videoService;
    @Autowired
    RedisService redisService;

    /**
     * 上传视频，获取视频的时长、大小以及视频的准备插图，存入redis，等待进一步操作
     *
     * @param multipartFile 上传视频
     * @return baseResult   返回上传结果
     */
    @RequestMapping(value = "/upLoadVideo", method = RequestMethod.GET)
    public BaseResult upLoadVideo(@RequestParam("multipartFile") MultipartFile multipartFile) {

        //判断上传文件是否非空
        if (multipartFile == null) {
            return new BaseResult(0, "上传失败！");
        }

        //从请求头拿到token，判断用户是否登录
        ServletRequestAttributes servletRequestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = servletRequestAttributes.getRequest();
        String token = request.getHeader("token");
        if (token == null || token == "") {
            return new BaseResult(0, "您没有登陆！");
        }

        //从token解析到用户信息，备用
        /*RedisUtils redisUtils = new RedisUtils();*/
        boolean b = redisUtils.hasKey(token);
        if (!b) {
            return new BaseResult(0, "用户信息已过期");
        }
        String s = redisUtils.get(token).toString();
        JwtUtils jwtUtils = new JwtUtils();
        String s1 = jwtUtils.VerifyToekn(s);
        User user = JSONObject.parseObject(s1, User.class);
        if (user != null) {
            return videoService.upLoadVideo(multipartFile, user);
        }
        return new BaseResult(0, "上传失败！");
    }

    /**
     * 根据用户的登录信息，去草稿箱（redis）拿到上传的视频集合信息
     *
     * @return 用户的上传的视频集合信息
     */
    @RequestMapping(value = "/findVideosInRedis", method = RequestMethod.GET)
    public BaseResult findVideoInRedis() {

        //从请求头拿到token，判断用户是否登录
        ServletRequestAttributes servletRequestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest httpServletRequest = servletRequestAttributes.getRequest();
        String token = httpServletRequest.getHeader("token");
        if (token == null || token == "") {
            return new BaseResult(0, "您没有登陆！");
        }

        //从token解析到用户信息，备用
        /*RedisUtils redisUtils = new RedisUtils();*/
        boolean b = redisUtils.hasKey(token);
        if (!b) {
            return new BaseResult(0, "用户信息已过期");
        }
        String s = redisUtils.get(token).toString();
        JwtUtils jwtUtils = new JwtUtils();
        String s1 = jwtUtils.VerifyToekn(s);
        User user = JSONObject.parseObject(s1, User.class);
        if (user != null) {
            return videoService.findVideosInRedis(user);
        }
        return new BaseResult(0, "查询失败！");
    }

    /**
     * 根据用户的登录信息，去草稿箱（redis）删除上传的视频集合信息
     *
     * @param videoPath 删除信息结果集
     * @return
     */
    @RequestMapping(value = "/delVideoInRedis", method = RequestMethod.GET)
    public BaseResult delVideoInRedis(@RequestParam("videoPath") String videoPath) {
        //判断不能为空
        if (videoPath == null || videoPath == "") {
            return new BaseResult(0, "视频路径不能为空或视频不存在！");
        }

        //从请求头拿到token，判断用户是否登录
        ServletRequestAttributes servletRequestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest httpServletRequest = servletRequestAttributes.getRequest();
        String token = httpServletRequest.getHeader("token");
        if (token == null || token == "") {
            return new BaseResult(0, "您没有登陆！");
        }

        //从token解析到用户信息，备用
        /*RedisUtils redisUtils = new RedisUtils();*/
        boolean b = redisUtils.hasKey(token);
        if (!b) {
            return new BaseResult(0, "用户信息已过期");
        }
        String s = redisUtils.get(token).toString();
        JwtUtils jwtUtils = new JwtUtils();
        String s1 = jwtUtils.VerifyToekn(s);
        User user = JSONObject.parseObject(s1, User.class);
        if (user != null) {
            return videoService.delVideoInRedis(user, videoPath);
        }
        return new BaseResult(0, "删除失败！");
    }

    /**
     * redis中查找视频的详细信息
     *
     * @param videoPath 上传的视频唯一路径
     * @return 该视频的信息
     */
    @RequestMapping(value = "/findOneInRedis", method = RequestMethod.GET)
    public BaseResult findOneInRedis(@RequestParam("videoPath") String videoPath) {
        //判断不能为空
        if (videoPath == null || videoPath == "") {
            return new BaseResult(0, "视频路径不能为空或视频不存在！");
        }

        //从请求头拿到token，判断用户是否登录
        ServletRequestAttributes servletRequestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest httpServletRequest = servletRequestAttributes.getRequest();
        String token = httpServletRequest.getHeader("token");
        if (token == null || token == "") {
            return new BaseResult(0, "您没有登陆！");
        }

        //从token解析到用户信息，备用
        /*RedisUtils redisUtils = new RedisUtils();*/
        boolean b = redisUtils.hasKey(token);
        if (!b) {
            return new BaseResult(0, "用户信息已过期");
        }
        String s = redisUtils.get(token).toString();
        JwtUtils jwtUtils = new JwtUtils();
        String s1 = jwtUtils.VerifyToekn(s);
        User user = JSONObject.parseObject(s1, User.class);
        if (user != null) {
            return videoService.findOneInRedis(user, videoPath);
        }
        return new BaseResult(0, "查询失败！");
    }

    /**
     * 上传图片作为封面
     *
     * @param multipartFile 封面照片文件
     * @param videoPath     对应上传的视频的路径
     * @return 上传成功后的回显，以及结果集
     */
    @RequestMapping(value = "/upLoadVideoImg", method = RequestMethod.GET)
    public BaseResult upLoadVideoImg(@RequestParam("multipartFile") MultipartFile multipartFile, @RequestParam("videoPath") String videoPath) {
        //判断非空
        if (multipartFile != null && videoPath != null && videoPath != "") {

            //从请求头拿到token，判断用户是否登录
            ServletRequestAttributes servletRequestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
            HttpServletRequest httpServletRequest = servletRequestAttributes.getRequest();
            String token = httpServletRequest.getHeader("token");
            if (token == null || token == "") {
                return new BaseResult(0, "您没有登陆！");
            }

            //从token解析到用户信息，备用
            /*RedisUtils redisUtils = new RedisUtils();*/
            boolean b = redisUtils.hasKey(token);
            if (!b) {
                return new BaseResult(0, "用户信息已过期");
            }
            String s = redisUtils.get(token).toString();
            JwtUtils jwtUtils = new JwtUtils();
            String s1 = jwtUtils.VerifyToekn(s);
            User user = JSONObject.parseObject(s1, User.class);
            if (user != null) {
                return videoService.upLoadVideoImg(user, videoPath, multipartFile);
            }
            return new BaseResult(0, "用户未登录！");
        }
        return new BaseResult(0, "上传失败！");
    }

    /**
     * 选择现成帧图作为封面
     *
     * @param videoImg  封面照片帧图
     * @param videoPath 对应上传的视频的路径
     * @return 上传成功后的回显，以及结果集
     */
    @RequestMapping(value = "/chooseVideoImg", method = RequestMethod.GET)
    public BaseResult chooseVideoImg(@RequestParam("videoImg") String videoImg, @RequestParam("videoPath") String videoPath) {
        //判断非空
        if (videoImg != null && videoImg != "" && videoPath != null && videoPath != "") {

            //从请求头拿到token，判断用户是否登录
            ServletRequestAttributes servletRequestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
            HttpServletRequest httpServletRequest = servletRequestAttributes.getRequest();
            String token = httpServletRequest.getHeader("token");
            if (token == null || token == "") {
                return new BaseResult(0, "您没有登陆！");
            }

            //从token解析到用户信息，备用
            /*RedisUtils redisUtils = new RedisUtils();*/
            boolean b = redisUtils.hasKey(token);
            if (!b) {
                return new BaseResult(0, "用户信息已过期");
            }
            String s = redisUtils.get(token).toString();
            JwtUtils jwtUtils = new JwtUtils();
            String s1 = jwtUtils.VerifyToekn(s);
            User user = JSONObject.parseObject(s1, User.class);
            if (user != null) {
                return videoService.chooseVideoImg(user, videoImg, videoPath);
            }
            return new BaseResult(0, "用户未登录！");
        }
        return new BaseResult(0, "上传失败！");
    }

    /**
     * 使用solr实现问题的搜索展示
     *
     * @param key 问题关键字
     * @return 包含关键字的结果集
     */
    @RequestMapping(value = "/findBySolrSearchQues", method = RequestMethod.GET)
    public BaseResult findBySolrSearchQues(@RequestParam("key") String key, @RequestParam("page") Integer page, @RequestParam("size") Integer size) {
        if (key == null || key == "") {
            return new BaseResult(0, "查询内容不能为空！");
        }
        //从请求头拿到token，判断用户是否登录
        ServletRequestAttributes servletRequestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest httpServletRequest = servletRequestAttributes.getRequest();
        String token = httpServletRequest.getHeader("token");
        if (token == null || token == "") {
            return new BaseResult(0, "您没有登陆！");
        }

        //从token解析到用户信息，备用
        /*RedisUtils redisUtils = new RedisUtils();*/
        boolean b = redisUtils.hasKey(token);
        if (!b) {
            return new BaseResult(0, "用户信息已过期");
        }
        String s = redisUtils.get(token).toString();
        JwtUtils jwtUtils = new JwtUtils();
        String s1 = jwtUtils.VerifyToekn(s);
        User user = JSONObject.parseObject(s1, User.class);
        if (user != null) {
            return videoService.findBySolrSearchQues(key, page, size);
        }
        return new BaseResult(0, "用户未登录！");
    }

    /**
     * 选择问题
     *
     * @param questionId solr里的问题id
     * @return 封装好的结果集
     */
    @RequestMapping(value = "/chooseSearchQuestion", method = RequestMethod.GET)
    public BaseResult chooseSearchQuestion(@RequestParam("questionId") Integer questionId) {
        if (questionId == null) {
            return new BaseResult(0, "选择问题失败！");
        }
        //从请求头拿到token，判断用户是否登录
        ServletRequestAttributes servletRequestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest httpServletRequest = servletRequestAttributes.getRequest();
        String token = httpServletRequest.getHeader("token");
        if (token == null || token == "") {
            return new BaseResult(0, "您没有登陆！");
        }

        //从token解析到用户信息，备用
        /*RedisUtils redisUtils = new RedisUtils();*/
        boolean b = redisUtils.hasKey(token);
        if (!b) {
            return new BaseResult(0, "用户信息已过期");
        }
        String s = redisUtils.get(token).toString();
        JwtUtils jwtUtils = new JwtUtils();
        String s1 = jwtUtils.VerifyToekn(s);
        User user = JSONObject.parseObject(s1, User.class);
        if (user != null) {
            return videoService.chooseSearchQuestion(questionId);
        }
        return new BaseResult(0, "用户未登录！");
    }

    /**
     * 将问题和描述插入草稿箱
     *
     * @param videoPath redis里的二级key
     * @param quesInfo  问题的描述
     * @return 封装好的结果集
     */
    @RequestMapping(value = "/insertSearchQuestion", method = RequestMethod.GET)
    public BaseResult insertSearchQuestion(@RequestParam("videoPath") String videoPath, @RequestParam("quesTitle") String quesTitle, @RequestParam("quesInfo") String quesInfo) {
        if (quesTitle == null || quesTitle == "" || videoPath == null || videoPath == "") {
            return new BaseResult(0, "选择问题失败！");
        }
        //从请求头拿到token，判断用户是否登录
        ServletRequestAttributes servletRequestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest httpServletRequest = servletRequestAttributes.getRequest();
        String token = httpServletRequest.getHeader("token");
        if (token == null || token == "") {
            return new BaseResult(0, "您没有登陆！");
        }

        //从token解析到用户信息，备用
        /*RedisUtils redisUtils = new RedisUtils();*/
        boolean b = redisUtils.hasKey(token);
        if (!b) {
            return new BaseResult(0, "用户信息已过期");
        }
        String s = redisUtils.get(token).toString();
        JwtUtils jwtUtils = new JwtUtils();
        String s1 = jwtUtils.VerifyToekn(s);
        User user = JSONObject.parseObject(s1, User.class);
        if (user != null) {
            return videoService.insertSearchQuestion(user, quesTitle, quesInfo, videoPath);
        }
        return new BaseResult(0, "用户未登录！");
    }

    /**
     * 使用solr实现问题的搜索展示
     *
     * @param key 问题关键字
     * @return 包含关键字的结果集
     */
    @RequestMapping(value = "/findTypeByTypeReq", method = RequestMethod.GET)
    public BaseResult findTypeByTypeReq(@RequestParam("key") String key, @RequestParam("page") Integer page, @RequestParam("size") Integer size) {
        if (key == null || key == "") {
            return new BaseResult(0, "查询内容不能为空！");
        }
        //从请求头拿到token，判断用户是否登录
        ServletRequestAttributes servletRequestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest httpServletRequest = servletRequestAttributes.getRequest();
        String token = httpServletRequest.getHeader("token");
        if (token == null || token == "") {
            return new BaseResult(0, "您没有登陆！");
        }

        //从token解析到用户信息，备用
        /*RedisUtils redisUtils = new RedisUtils();*/
        boolean b = redisUtils.hasKey(token);
        if (!b) {
            return new BaseResult(0, "用户信息已过期");
        }
        String s = redisUtils.get(token).toString();
        JwtUtils jwtUtils = new JwtUtils();
        String s1 = jwtUtils.VerifyToekn(s);
        User user = JSONObject.parseObject(s1, User.class);
        if (user != null) {
            return videoService.findTypeByTypeReq(key, page, size);
        }
        return new BaseResult(0, "用户未登录！");
    }

    /**
     * 绑定话题——添加视频或者问题的标签
     *
     * @param videoPath redis里的二级key
     * @param typeId    插入的标签的id
     * @return 封装好的结果集
     */
    @RequestMapping(value = "/insertSearchType", method = RequestMethod.GET)
    public BaseResult insertSearchType(@RequestParam("videoPath") String videoPath, @RequestParam("typeId") Integer typeId) {
        if (videoPath == null || videoPath == "" || typeId == null) {
            return new BaseResult(0, "未选择标签或未上传视频！");
        }
        //从请求头拿到token，判断用户是否登录
        ServletRequestAttributes servletRequestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest httpServletRequest = servletRequestAttributes.getRequest();
        String token = httpServletRequest.getHeader("token");
        if (token == null || token == "") {
            return new BaseResult(0, "您没有登陆！");
        }

        //从token解析到用户信息，备用
        /*RedisUtils redisUtils = new RedisUtils();*/
        boolean b = redisUtils.hasKey(token);
        if (!b) {
            return new BaseResult(0, "用户信息已过期");
        }
        String s = redisUtils.get(token).toString();
        JwtUtils jwtUtils = new JwtUtils();
        String s1 = jwtUtils.VerifyToekn(s);
        User user = JSONObject.parseObject(s1, User.class);
        if (user != null) {
            return videoService.insertSearchType(user, videoPath, typeId);
        }
        return new BaseResult(0, "用户未登录！");
    }

    /**
     * 查看选择好的视频或者问题 的标签
     *
     * @param videoPath redis里的二级key
     * @return 封装好的结果集
     */
    @RequestMapping(value = "/findTypeByRedis", method = RequestMethod.GET)
    public BaseResult findTypeByRedis(@RequestParam("videoPath") String videoPath) {
        if (videoPath == null || videoPath == "") {
            return new BaseResult(0, "查询内容不能为空！");
        }
        //从请求头拿到token，判断用户是否登录
        ServletRequestAttributes servletRequestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest httpServletRequest = servletRequestAttributes.getRequest();
        String token = httpServletRequest.getHeader("token");
        if (token == null || token == "") {
            return new BaseResult(0, "您没有登陆！");
        }

        //从token解析到用户信息，备用
        /*RedisUtils redisUtils = new RedisUtils();*/
        boolean b = redisUtils.hasKey(token);
        if (!b) {
            return new BaseResult(0, "用户信息已过期");
        }
        String s = redisUtils.get(token).toString();
        JwtUtils jwtUtils = new JwtUtils();
        String s1 = jwtUtils.VerifyToekn(s);
        User user = JSONObject.parseObject(s1, User.class);
        if (user != null) {
            return videoService.findTypeByRedis(user, videoPath);
        }
        return new BaseResult(0, "用户未登录！");
    }

    /**
     * 删除选择好的视频或者问题 的标签
     *
     * @param videoPath redis里的二级key
     * @return 封装好的结果集
     */
    @RequestMapping(value = "/delTypeByRedis", method = RequestMethod.GET)
    public BaseResult delTypeByRedis(@RequestParam("videoPath") String videoPath, @RequestParam("typeId") Integer typeId) {
        if (videoPath == null || videoPath == "") {
            return new BaseResult(0, "查询内容不能为空！");
        }
        //从请求头拿到token，判断用户是否登录
        ServletRequestAttributes servletRequestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest httpServletRequest = servletRequestAttributes.getRequest();
        String token = httpServletRequest.getHeader("token");
        if (token == null || token == "") {
            return new BaseResult(0, "您没有登陆！");
        }

        //从token解析到用户信息，备用
        /*RedisUtils redisUtils = new RedisUtils();*/
        boolean b = redisUtils.hasKey(token);
        if (!b) {
            return new BaseResult(0, "用户信息已过期");
        }
        String s = redisUtils.get(token).toString();
        JwtUtils jwtUtils = new JwtUtils();
        String s1 = jwtUtils.VerifyToekn(s);
        User user = JSONObject.parseObject(s1, User.class);
        if (user != null) {
            return videoService.delTypeByRedis(user, videoPath, typeId);
        }
        return new BaseResult(0, "用户未登录！");
    }

    /**
     * 修改标题
     *
     * @param videoPath redis里的二级key
     * @param viTitle   修改后的标题
     * @return 封装好的结果集
     */
    @RequestMapping(value = "/updateNewTitle", method = RequestMethod.GET)
    public BaseResult updateNewTitle(@RequestParam("videoPath") String videoPath, @RequestParam("viTitle") String viTitle) {
        if (videoPath == null || videoPath == "" || viTitle == null || viTitle == "") {
            return new BaseResult(0, "请修改标题或未上传视频！");
        }
        //从请求头拿到token，判断用户是否登录
        ServletRequestAttributes servletRequestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest httpServletRequest = servletRequestAttributes.getRequest();
        String token = httpServletRequest.getHeader("token");
        if (token == null || token == "") {
            return new BaseResult(0, "您没有登陆！");
        }

        //从token解析到用户信息，备用
        /*RedisUtils redisUtils = new RedisUtils();*/
        boolean b = redisUtils.hasKey(token);
        if (!b) {
            return new BaseResult(0, "用户信息已过期");
        }
        String s = redisUtils.get(token).toString();
        JwtUtils jwtUtils = new JwtUtils();
        String s1 = jwtUtils.VerifyToekn(s);
        User user = JSONObject.parseObject(s1, User.class);
        if (user != null) {
            return videoService.updateNewTitle(user, videoPath, viTitle);
        }
        return new BaseResult(0, "用户未登录！");
    }

    /**
     * 修改标题
     *
     * @param videoPath redis里的二级key
     * @param viInfo    修改后的内容详情
     * @return 封装好的结果集
     */
    @RequestMapping(value = "/updateNewInfo", method = RequestMethod.GET)
    public BaseResult updateNewInfo(@RequestParam("videoPath") String videoPath, @RequestParam("viInfo") String viInfo) {
        if (videoPath == null || videoPath == "" || viInfo == null || viInfo == "") {
            return new BaseResult(0, "请修改详情或未上传视频！");
        }
        //从请求头拿到token，判断用户是否登录
        ServletRequestAttributes servletRequestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest httpServletRequest = servletRequestAttributes.getRequest();
        String token = httpServletRequest.getHeader("token");
        if (token == null || token == "") {
            return new BaseResult(0, "您没有登陆！");
        }

        //从token解析到用户信息，备用
        /*RedisUtils redisUtils = new RedisUtils();*/
        boolean b = redisUtils.hasKey(token);
        if (!b) {
            return new BaseResult(0, "用户信息已过期");
        }
        String s = redisUtils.get(token).toString();
        JwtUtils jwtUtils = new JwtUtils();
        String s1 = jwtUtils.VerifyToekn(s);
        User user = JSONObject.parseObject(s1, User.class);
        if (user != null) {
            return videoService.updateNewInfo(user, videoPath, viInfo);
        }
        return new BaseResult(0, "用户未登录！");
    }

    /**
     * 修改标题
     *
     * @param videoPath        redis里的二级key
     * @param autherAndForward 原创还是转发
     * @return 封装好的结果集
     */
    @RequestMapping(value = "/updateAutherAndForward", method = RequestMethod.GET)
    public BaseResult updateAutherAndForward(@RequestParam("videoPath") String videoPath, @RequestParam("autherAndForward") Integer autherAndForward) {
        if (videoPath == null || videoPath == "" || autherAndForward == null) {
            return new BaseResult(0, "请选择原创或者转发或未上传视频！");
        }
        //从请求头拿到token，判断用户是否登录
        ServletRequestAttributes servletRequestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest httpServletRequest = servletRequestAttributes.getRequest();
        String token = httpServletRequest.getHeader("token");
        if (token == null || token == "") {
            return new BaseResult(0, "您没有登陆！");
        }

        //从token解析到用户信息，备用
        /*RedisUtils redisUtils = new RedisUtils();*/
        boolean b = redisUtils.hasKey(token);
        if (!b) {
            return new BaseResult(0, "用户信息已过期");
        }
        String s = redisUtils.get(token).toString();
        JwtUtils jwtUtils = new JwtUtils();
        String s1 = jwtUtils.VerifyToekn(s);
        User user = JSONObject.parseObject(s1, User.class);
        if (user != null) {
            return videoService.updateAutherAndForward(user, videoPath, autherAndForward);
        }
        return new BaseResult(0, "用户未登录！");
    }

    /**
     * 提交表单，插入数据库
     *
     * @param viCreatetime
     * @param videoPath
     * @return
     */
    @RequestMapping(value = "/insertVideoToDataBase", method = RequestMethod.GET)
    public BaseResult insertVideoToDataBase(@RequestParam("viCreatetime") Date viCreatetime, @RequestParam("videoPath") String videoPath) {
        if (videoPath == null || videoPath == "" || viCreatetime == null) {
            return new BaseResult(0, "请选择上传时间或者未上传视频！");
        }
        //从请求头拿到token，判断用户是否登录
        ServletRequestAttributes servletRequestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest httpServletRequest = servletRequestAttributes.getRequest();
        String token = httpServletRequest.getHeader("token");
        if (token == null || token == "") {
            return new BaseResult(0, "您没有登陆！");
        }

        //从token解析到用户信息，备用
        /*RedisUtils redisUtils = new RedisUtils();*/
        boolean b = redisUtils.hasKey(token);
        if (!b) {
            return new BaseResult(0, "用户信息已过期");
        }
        String s = redisUtils.get(token).toString();
        JwtUtils jwtUtils = new JwtUtils();
        String s1 = jwtUtils.VerifyToekn(s);
        User user = JSONObject.parseObject(s1, User.class);
        if (user != null) {
            return videoService.insertVideoToDataBase(user, videoPath, viCreatetime);
        }
        return new BaseResult(0, "用户未登录！");
    }

    /**
     * 将用户看过的视频放入浏览记录
     *
     * @param videoId 视频id
     */
    @RequestMapping(value = "/recordViewHistory", method = RequestMethod.GET)
    public BaseResult recordViewHistory(@RequestParam("videoId") Integer videoId) {

        //从请求头拿到token，判断用户是否登录
        ServletRequestAttributes servletRequestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest httpServletRequest = servletRequestAttributes.getRequest();
        String token = httpServletRequest.getHeader("token");
        if (token == null || token == "") {
            return new BaseResult(0, "您没有登陆！");
        }

        //从token解析到用户信息，备用
        /*RedisUtils redisUtils = new RedisUtils();*/
        boolean b = redisUtils.hasKey(token);
        if (!b) {
            return new BaseResult(0, "用户信息已过期");
        }
        String s = redisUtils.get(token).toString();
        JwtUtils jwtUtils = new JwtUtils();
        String s1 = jwtUtils.VerifyToekn(s);
        User user = JSONObject.parseObject(s1, User.class);
        if (user != null) {
            return videoService.recordViewHistory(user, videoId);
        }
        return new BaseResult(0, "用户未登录！");
    }

    /**
     * 根据用户的浏览记录和喜好从而向用户推荐视频
     *
     * @return 推荐的视频结果集
     */
    @RequestMapping(value = "/recommendVideos", method = RequestMethod.GET)
    public BaseResult recommendVideos() {
        //从请求头拿到token，判断用户是否登录
        ServletRequestAttributes servletRequestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest httpServletRequest = servletRequestAttributes.getRequest();
        String token = httpServletRequest.getHeader("token");
        if (token == null || token == "") {
            return videoService.recommendVideos();
        }

        //从token解析到用户信息，备用
        /*RedisUtils redisUtils = new RedisUtils();*/
        boolean b = redisUtils.hasKey(token);
        if (!b) {
            return videoService.recommendVideos();
        }
        String s = redisUtils.get(token).toString();
        JwtUtils jwtUtils = new JwtUtils();
        String s1 = jwtUtils.VerifyToekn(s);
        User user = JSONObject.parseObject(s1, User.class);
        if (user != null) {
            return videoService.recommendVideos(user);
        }
        return null;
    }

    /**
     * 展示具体的某一个视频
     *
     * @param videoId 视频id
     * @return 视频详情结果集
     */
    @RequestMapping(value = "/findVideoByVideoId", method = RequestMethod.GET)
    public BaseResult findVideoByVideoId(@RequestParam("videoId") Integer videoId) {

        if (videoId != null) {
            //从请求头拿到token，判断用户是否登录
            ServletRequestAttributes servletRequestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
            HttpServletRequest httpServletRequest = servletRequestAttributes.getRequest();
            String token = httpServletRequest.getHeader("token");
            if (token == null || token == "") {
                return videoService.findVideoByVideoId(videoId);
            }

            //从token解析到用户信息，备用
            /*RedisUtils redisUtils = new RedisUtils();*/
            boolean b = redisUtils.hasKey(token);
            if (!b) {
                return videoService.findVideoByVideoId(videoId);
            }
            String s = redisUtils.get(token).toString();
            JwtUtils jwtUtils = new JwtUtils();
            String s1 = jwtUtils.VerifyToekn(s);
            User user = JSONObject.parseObject(s1, User.class);
            if (user != null) {
                return videoService.findVideoByVideoId(user, videoId);
            }
        }
        return new BaseResult(0, "查询失败！");
    }

        /**
         * 展示用户发的视频
         * @return 视频结果集
         */
        @RequestMapping(value = "/findVideoByUserId", method = RequestMethod.GET)
        public BaseResult findVideoByUserId () {
            //从请求头拿到token，判断用户是否登录
            ServletRequestAttributes servletRequestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
            HttpServletRequest httpServletRequest = servletRequestAttributes.getRequest();
            String token = httpServletRequest.getHeader("token");
            if (token == null || token == "") {
                return new BaseResult(0, "您没有登陆！");
            }

            //从token解析到用户信息，备用
            /*RedisUtils redisUtils = new RedisUtils();*/
            boolean b = redisUtils.hasKey(token);
            if (!b) {
                return new BaseResult(0, "用户信息已过期");
            }
            String s = redisUtils.get(token).toString();
            JwtUtils jwtUtils = new JwtUtils();
            String s1 = jwtUtils.VerifyToekn(s);
            User user = JSONObject.parseObject(s1, User.class);
            if (user != null) {
                return videoService.findVideoByUserId(user);
            }
            return new BaseResult(0, "用户未登录！");
        }
    }
