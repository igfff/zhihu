package com.zhihu.controller;

import com.zhihu.pojo.baseVo.BaseResult;
import com.zhihu.service.SolrService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/search")
public class SolrController {

        @Autowired
        SolrService solrService;

        /**
         * 默认展示当前的最热问题
         * @return 问题结果集
         */
        @RequestMapping(value = "/findHotTopic",method = RequestMethod.GET)
        public BaseResult findHotTopic(){
                return solrService.findHotTopic();
        }

        /**
         * 根据关键字搜索与关键字相关的问题
         * @return
         */
        @RequestMapping(value = "/findQuesByKeyword",method = RequestMethod.GET)
        public BaseResult findQuesByKeyword(@RequestParam("keyword") String keyword){
                return solrService.findQuesByKeyword(keyword);
        }
}
