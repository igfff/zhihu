package com.zhihu.config;

import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

@Component
@Configuration
public class SolrConfig {

    @Value("${spring.data.solr.host}")
    private String host;

        @Bean(name = "solrClient")
        public HttpSolrClient httpSolrClient(){
            HttpSolrClient.Builder builder = new HttpSolrClient.Builder(host);
            return builder.build();
        }
}
