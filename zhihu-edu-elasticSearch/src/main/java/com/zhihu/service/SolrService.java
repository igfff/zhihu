package com.zhihu.service;

import com.zhihu.pojo.baseVo.BaseResult;

public interface SolrService {

    /**
     * 默认展示当前的最热问题
     * @return 问题结果集
     */
    BaseResult findHotTopic();

    /**
     * 根据关键字搜索与关键字相关的问题
     * @return
     */
    BaseResult findQuesByKeyword(String keyword);
}
