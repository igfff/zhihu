package com.zhihu.client;

import com.zhihu.pojo.Question;
import com.zhihu.pojo.baseVo.BaseResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "zhihu-edu-question",path = "/question")
public interface QuestionClients {

    @RequestMapping(value = "/findQuestionById",method = RequestMethod.GET)
    public BaseResult findQuestionById(@RequestParam("questionId") Integer questionId);

    //通过传过来的实体更新question数据库信息
    @RequestMapping(value = "/updateQuestion",method = RequestMethod.POST)
    public BaseResult updateQuestion(@RequestBody Question question);
}
