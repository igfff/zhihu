package com.zhihu.client;

import com.zhihu.pojo.Answer;
import com.zhihu.pojo.Question;
import com.zhihu.pojo.baseVo.BaseResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "zhihu-edu-answer",path = "/answer")

public interface AnswerClients {

    //根据问题的id查询问题
    @RequestMapping(value = "/findAnswerById",method = RequestMethod.GET)
    public BaseResult findAnswerById(@RequestParam("answerId") Integer answerId);

    //通过传过来的实体更新answer数据库信息
    @RequestMapping(value = "/updateAnswer",method = RequestMethod.POST)
    public BaseResult updateAnswer(@RequestBody Answer answer);
}
