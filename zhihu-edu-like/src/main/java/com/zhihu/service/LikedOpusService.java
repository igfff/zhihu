package com.zhihu.service;


import com.zhihu.pojo.LikeOpus;
import com.zhihu.pojo.LikedOpusCount;
import com.zhihu.pojo.baseVo.BaseResult;


import java.util.List;

public interface LikedOpusService {

    /**
     * 保存点赞记录
     *
     * @param likeOpus
     * @return
     */
    LikeOpus save(LikeOpus likeOpus);

    /**
     * 批量保存或修改
     *
     * @param list
     */
    List<LikeOpus> saveAll(List<LikeOpus> list);


    /**
     * 根据被点赞人的id查询点赞列表（即查询都谁给这个人点赞过）
     *
     * @param likedUserId 被点赞人的id
     * @return
     */
    List<LikeOpus> getLikedListByLikedUserId(String likedUserId);

    /**
     * 根据点赞人的id查询点赞列表（即查询这个人都给谁点赞过）
     *
     * @param likedPostId
     * @return
     */
    List<LikeOpus> getLikedListByLikedPostId(String likedPostId);

    /**
     * 通过被点赞人和点赞人id查询是否存在点赞记录
     *
     * @param likedUserId
     * @param likedPostId
     * @return
     */
    LikeOpus getByLikedUserIdAndLikedPostId(String likedUserId, String likedPostId,String followOpusId,String followOpusTap);

    /**
     * 将Redis里的点赞数据存入数据库中
     */
    void transLikedFromRedis2DB();

    /**
     * 将Redis中的点赞数量数据存入数据库
     */
    void transLikedCountFromRedis2DB();

    /**
     * 从mysql中查询当前用户所有的点赞回答
     * @param userId
     */
    List getUserLikeData(Integer userId);
}
