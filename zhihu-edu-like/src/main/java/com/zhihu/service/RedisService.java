package com.zhihu.service;

import com.zhihu.pojo.LikeOpus;
import com.zhihu.pojo.LikedOpusCount;

import java.util.List;

public interface RedisService {

    /**
     * 作品+评论点赞。状态为1
     *
     * @param likedUserId
     * @param likedPostId
     */
    void saveLikedOpusRedis(String likedUserId, String likedPostId,String followOpusId,String followOpusTap);

    /**
     * 作品+评论取消点赞。将状态改变为0
     *
     * @param likedUserId
     * @param likedPostId
     */
    void unlikeOpusFromRedis(String likedUserId, String likedPostId,String followOpusId,String followOpusTap);



    //void saveLikedCommRedis(String likedUserId, String likedPostId,Integer lcCommentId);


    //void unlikeCommFromRedis(String likedUserId, String likedPostId,Integer lcCommentId);



    /**
     * 从redis中删除一条作品的点赞信息
     * @param likedUserId
     * @param likedPostId
     * @param followOpusId
     * @param followOpusTap
     */
    void deleteLikedOpusFromRedis(String likedUserId, String likedPostId,String followOpusId,String followOpusTap);



    //void deleteLikedCommFromRedis(String likedUserId, String likedPostId,Integer lcCommentId);


    /**
     * 该用户的作品或评论点赞数加1
     * @param followOpusId
     * @param followOpusTap
     */
    void incrementLikedOpusCount(String followOpusTap,String followOpusId);

    /**
     * 该用户的作品或评论点赞数减1
     * @param followOpusId
     * @param followOpusTap
     */
    void decrementLikedOpusCount(String followOpusTap,String followOpusId);



    //void incrementLikedCommCount(String commentsId);


    //void decrementLikedCommCount(String commentsId);

    /**
     * 获取Redis中存储的所有作品的点赞数据
     *
     * @return
     */
    List<LikeOpus> getLikedOpusDataFromRedis();



    //List<LikeComm> getLikedCommDataFromRedis();

    /**
     * 获取Redis中存储的所有作品的点赞数量
     *
     * @return
     */
    List<LikedOpusCount> getLikedOpusCountFromRedis();



    //List<LikedCommCount> getLikedCommCountFromRedis();*/

    /**
     * 从Redis中获取某个作品的点赞数量
     * @param likedOpusCount
     * @return
     */
    Integer getOpusLikeNums(LikedOpusCount likedOpusCount);

    /**
     * 往redis中添加某个作品的点赞数量
     * @param followOpusTap
     * @param followOpusId
     */
    void insertOpusLikeNums(Integer followOpusTap, Integer followOpusId);

}
