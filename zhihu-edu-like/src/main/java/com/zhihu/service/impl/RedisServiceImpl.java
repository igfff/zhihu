package com.zhihu.service.impl;

import com.zhihu.Enums.LikedStatusEnum;
import com.zhihu.pojo.LikeOpus;
import com.zhihu.pojo.LikedOpusCount;
import com.zhihu.service.LikedOpusService;
import com.zhihu.service.RedisService;
import com.zhihu.utils.RedisKeyUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.Cursor;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ScanOptions;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class RedisServiceImpl implements RedisService {

    @Autowired
    RedisTemplate redisTemplate;

    //作品或者评论点赞，状态为1,(redis:value=1)
    @Override
    public void saveLikedOpusRedis(String likedUserId, String likedPostId,String followOpusId,String followOpusTap) {
        String key = RedisKeyUtils.getLikedOpusKey(likedUserId, likedPostId,followOpusId,followOpusTap);
        redisTemplate.opsForHash().put(RedisKeyUtils.MAP_USER_OPUS_LIKED, key, LikedStatusEnum.LIKE.getCode());
    }

    //作品或者评论取消点赞，状态为0,(redis:value=0)
    @Override
    public void unlikeOpusFromRedis(String likedUserId, String likedPostId,String followOpusId,String followOpusTap) {
        String key = RedisKeyUtils.getLikedOpusKey(likedUserId, likedPostId,followOpusId,followOpusTap);
        redisTemplate.opsForHash().put(RedisKeyUtils.MAP_USER_OPUS_LIKED, key, LikedStatusEnum.UNLIKE.getCode());
    }


/*    //评论点赞，状态为1,(redis:value=1)
    @Override
    public void saveLikedCommRedis(String likedUserId, String likedPostId,Integer lcCommentId) {
        String key = RedisKeyUtils.getLikedCommKey(likedUserId, likedPostId,lcCommentId);
        redisTemplate.opsForHash().put(RedisKeyUtils.MAP_USER_COMM_LIKED, key, LikedStatusEnum.LIKE.getCode());
    }

    //评论取消点赞，状态为0,(redis:value=0)
    @Override
    public void unlikeCommFromRedis(String likedUserId, String likedPostId,Integer lcCommentId) {
        String key = RedisKeyUtils.getLikedCommKey(likedUserId, likedPostId,lcCommentId);
        redisTemplate.opsForHash().put(RedisKeyUtils.MAP_USER_COMM_LIKED, key, LikedStatusEnum.UNLIKE.getCode());
    }*/


    //作品从redis中删除一条作品的点赞的信息，通过key值
    @Override
    public void deleteLikedOpusFromRedis(String likedUserId, String likedPostId,String followOpusId,String followOpusTap) {
        String key = RedisKeyUtils.getLikedOpusKey(likedUserId, likedPostId,followOpusId,followOpusTap);
        redisTemplate.opsForHash().delete(RedisKeyUtils.MAP_USER_OPUS_LIKED, key);
    }

/*    //作品从redis中删除一条评论的点赞信息， 通过key值
    @Override
    public void deleteLikedCommFromRedis(String likedUserId, String likedPostId,Integer lcCommentId) {
        String key = RedisKeyUtils.getLikedCommKey(likedUserId, likedPostId, lcCommentId);
        redisTemplate.opsForHash().delete(RedisKeyUtils.MAP_USER_COMM_LIKED, key);
    }*/

    //该作品评论点赞数加1
    @Override
    public void incrementLikedOpusCount(String followOpusTap,String followOpusId) {
        String key = RedisKeyUtils.getLikedOpusCountKey(followOpusTap, followOpusId);
        redisTemplate.opsForHash().increment(RedisKeyUtils.MAP_USER_OPUS_COUNT, key, 1);
    }

    //该作品点赞数-1
    @Override
    public void decrementLikedOpusCount(String followOpusTap,String followOpusId) {
        String key = RedisKeyUtils.getLikedOpusCountKey(followOpusTap, followOpusId);
        redisTemplate.opsForHash().increment(RedisKeyUtils.MAP_USER_OPUS_COUNT, key, -1);
    }


/*    //该用户的评论点赞数加1
    @Override
    public void incrementLikedCommCount(String commentsId) {
        redisTemplate.opsForHash().increment(RedisKeyUtils.MAP_USER_COMM_COUNT, commentsId, 1);
    }

    //该用户的评论点赞数-1
    @Override
    public void decrementLikedCommCount(String commentsId) {
        redisTemplate.opsForHash().increment(RedisKeyUtils.MAP_USER_COMM_COUNT, commentsId, -1);
    }*/

    //获取redis中所有作品的点赞数据
    @Override
    public List<LikeOpus> getLikedOpusDataFromRedis() {
        Cursor<Map.Entry<Object, Object>> cursor = redisTemplate.opsForHash().scan(RedisKeyUtils.MAP_USER_OPUS_LIKED, ScanOptions.NONE);
        List<LikeOpus> list = new ArrayList<>();
        while (cursor.hasNext()) {
            Map.Entry<Object, Object> entry = cursor.next();
            String key = (String) entry.getKey();
            //分离出 likedUserId，likedPostId
            String[] split = key.split("::");
            String likedUserId = split[0];
            String likedPostId = split[1];
            String followOpusId = split[2];
            String followOpusTap = split[3];
            Integer likeStatus = (Integer) entry.getValue();

            //组装成 LikeOpus 对象
            LikeOpus likeOpus = new LikeOpus(likedUserId, likedPostId, followOpusId,followOpusTap,likeStatus);
            list.add(likeOpus);

            //存到 list 后从 Redis 中删除
            //redisTemplate.opsForHash().delete(RedisKeyUtils.MAP_USER_OPUS_LIKED, key);
        }
        return list;
    }

/*    //获取redis中所有评论的点赞数据
    @Override
    public List<LikeComm> getLikedCommDataFromRedis() {
        Cursor<Map.Entry<Object, Object>> cursor = redisTemplate.opsForHash().scan(RedisKeyUtils.MAP_USER_COMM_LIKED, ScanOptions.NONE);
        List<LikeComm> list = new ArrayList<>();
        while (cursor.hasNext()) {
            Map.Entry<Object, Object> entry = cursor.next();
            String key = (String) entry.getKey();
            //分离出 likedUserId，likedPostId
            String[] split = key.split("::");
            String likedUserId = split[0];
            String likedPostId = split[1];
            String lcCommentId = split[2];
            Integer lcStatus = (Integer) entry.getValue();

            //组装成 LikeOpus 对象
            LikeComm likeComm = new LikeComm(likedUserId, likedPostId,lcCommentId, lcStatus);
            list.add(likeComm);

            //存到 list 后从 Redis 中删除
            redisTemplate.opsForHash().delete(RedisKeyUtils.MAP_USER_COMM_LIKED, key);
        }
        return list;
    }*/

    //获取Redis中存储的所有作品的点赞数量
    @Override
    public List<LikedOpusCount> getLikedOpusCountFromRedis() {
        Cursor<Map.Entry<Object, Object>> cursor = redisTemplate.opsForHash().scan(RedisKeyUtils.MAP_USER_OPUS_COUNT, ScanOptions.NONE);
        List<LikedOpusCount> list = new ArrayList<>();
        while (cursor.hasNext()) {
            Map.Entry<Object, Object> map = cursor.next();
            //将点赞数量存储在 LikedCount中
            String key = (String) map.getKey();
            String[] split = key.split("::");
            String followOpusTap= split[0];
            String followOpusId  = split[1];
            Integer count = (Integer) map.getValue();
            LikedOpusCount likedOpusCount = new LikedOpusCount(followOpusTap,followOpusId,count);

            list.add(likedOpusCount);
            //从Redis中删除这条记录
            //redisTemplate.opsForHash().delete(RedisKeyUtils.MAP_USER_OPUS_COUNT, key);
        }
        return list;
    }

/*    //获取Redis中存储的所有评论的点赞数量
    @Override
    public List<LikedCommCount> getLikedCommCountFromRedis() {
        Cursor<Map.Entry<Object, Object>> cursor = redisTemplate.opsForHash().scan(RedisKeyUtils.MAP_USER_COMM_COUNT, ScanOptions.NONE);
        List<LikedCommCount> list = new ArrayList<>();
        while (cursor.hasNext()) {
            Map.Entry<Object, Object> map = cursor.next();
            //将点赞数量存储在 LikedCount中
            String key = (String) map.getKey();
            Integer count = (Integer) map.getValue();
            LikedCommCount likedCommCount = new LikedCommCount(key,count);

            list.add(likedCommCount);
            //从Redis中删除这条记录
            redisTemplate.opsForHash().delete(RedisKeyUtils.MAP_USER_COMM_COUNT, key);
        }
        return list;
    }*/


    //获取某作品的点赞数
    @Override
    public Integer getOpusLikeNums(LikedOpusCount likedOpusCount) {
        String key = RedisKeyUtils.getLikedOpusCountKey(likedOpusCount.getFollowOpusTap(), likedOpusCount.getFollowOpusId());
        Integer count  = (Integer)redisTemplate.opsForHash().get(RedisKeyUtils.MAP_USER_OPUS_COUNT, key);
        if (count!=null){
            return count;
        }
        return 0;
    }

    //添加一条作品的点赞数量到redis中
    @Override
    public void insertOpusLikeNums(Integer followOpusTap, Integer followOpusId) {
        String key = RedisKeyUtils.getLikedOpusCountKey(followOpusTap.toString(), followOpusId.toString());
        redisTemplate.opsForHash().put(RedisKeyUtils.MAP_USER_OPUS_COUNT,key,0);
        System.out.println("添加成功！");
    }
}
