package com.zhihu.controller;


import com.zhihu.pojo.LikeOpus;
import com.zhihu.pojo.LikedOpusCount;
import com.zhihu.pojo.baseVo.BaseResult;
import com.zhihu.service.LikedOpusService;
import com.zhihu.service.RedisService;
import com.zhihu.utils.RedisKeyUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/like")
@RestController
public class LikeController {

    @Autowired
    RedisService redisService;
    @Autowired
    RedisTemplate redisTemplate;

    @Autowired
    LikedOpusService likedOpusService;

    //点赞或者取消点赞的接口
    @RequestMapping(value = "/addOrCancelLike",method = RequestMethod.POST)
    public BaseResult addLike(@RequestBody LikeOpus likeOpus){
        String likedUserId = likeOpus.getLikedUserId();
        String likedPostId = likeOpus.getLikedPostId();
        String followOpusId = likeOpus.getFollowOpusId();
        String followOpusTap = likeOpus.getFollowOpusTap();
        System.out.println(likeOpus);

        BaseResult baseResult = new BaseResult();
        String key = RedisKeyUtils.getLikedOpusKey(likedUserId, likedPostId, followOpusId, followOpusTap);
        System.out.println(key);
        Boolean hasKey = redisTemplate.opsForHash().hasKey(RedisKeyUtils.MAP_USER_OPUS_LIKED,key);
        System.out.println(hasKey);
        if (hasKey){
            Integer value = (Integer)redisTemplate.opsForHash().get(RedisKeyUtils.MAP_USER_OPUS_LIKED,key);
            System.out.println(value);
            //如果redis中该key值的value为1，那么用户点击button触发的就是取消点赞的流程
            if (value==1){
                //将数据库中该key值的value值改为0
                redisService.unlikeOpusFromRedis(likedUserId,likedPostId,followOpusId,followOpusTap);
                redisService.decrementLikedOpusCount(followOpusTap,followOpusId);
                likedOpusService.transLikedFromRedis2DB();
                likedOpusService.transLikedCountFromRedis2DB();
                baseResult.setCode(1);
                baseResult.setMsg("取消点赞成功！");
                return baseResult;
            }else {
                //如果redis中该key值的value不为1,那么用户点击button触发的就是点赞的流程
                redisService.saveLikedOpusRedis(likedUserId,likedPostId,followOpusId,followOpusTap);
                redisService.incrementLikedOpusCount(followOpusTap,followOpusId);
                likedOpusService.transLikedFromRedis2DB();
                likedOpusService.transLikedCountFromRedis2DB();
                baseResult.setCode(0);
                baseResult.setMsg("点赞成功！");
                return baseResult;
            }
        }else {
            //数据库中没有该key值，先把数据存到Redis里,再定时存回数据库
            redisService.saveLikedOpusRedis(likedUserId, likedPostId, followOpusId, followOpusTap);
            redisService.incrementLikedOpusCount(followOpusTap,followOpusId);
            likedOpusService.transLikedFromRedis2DB();
            likedOpusService.transLikedCountFromRedis2DB();
            baseResult.setCode(0);
            baseResult.setMsg("点赞成功");
            return baseResult;
        }
    }


/*    @RequestMapping(value = "/unLike",method = RequestMethod.POST)
    public BaseResult unLike(@RequestParam("likedUserId") String likedUserId,
                             @RequestParam("likedPostId") String likedPostId,
                             @RequestParam("likedUserId") String followOpusId,
                             @RequestParam("likedPostId") String followOpusTap){
        BaseResult baseResult = new BaseResult();
        //先把取消点赞的数据，存到Redis里,再定时存回数据库
        redisService.unlikeOpusFromRedis(likedUserId,likedPostId,followOpusId,followOpusTap);
        redisService.decrementLikedOpusCount(followOpusId,followOpusTap);
        baseResult.setCode(0);
        baseResult.setMsg("取消点赞成功");
        return baseResult;
    }*/

    //从redis中获取作品点赞数的接口
    @RequestMapping(value = "/getOpusLikeNums",method = RequestMethod.POST)
    public Integer getOpusLikeNums(@RequestBody LikedOpusCount likedOpusCount){
        return redisService.getOpusLikeNums(likedOpusCount);
    }

    //往redis中添加一条作品的点赞数量接口
    @RequestMapping(value = "/insertOpusLikeNums",method = RequestMethod.GET)
    public void insertOpusLikeNums(@RequestParam("followOpusTap")Integer followOpusTap,@RequestParam("followOpusId")Integer followOpusId){
        redisService.insertOpusLikeNums(followOpusTap,followOpusId);
    }

    //往redis中获取某个用户所有关于回答的点赞信息
    @RequestMapping(value = "/getUserLikeData",method = RequestMethod.GET)
    public List getUserLikeData(@RequestParam("userId")Integer userId){
        return likedOpusService.getUserLikeData(userId);
    }
}
