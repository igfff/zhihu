package com.zhihu.dao;

import com.zhihu.pojo.LikeOpus;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface LikeRepository extends JpaRepository<LikeOpus,Integer> {

    List<LikeOpus> findByLikedUserIdAndLikeStatus(String likedUserId, Integer code);

    List<LikeOpus> findByLikedPostIdAndLikeStatus(String likedPostId, Integer code);

    LikeOpus getByLikedUserIdAndLikedPostIdAndFollowOpusIdAndFollowOpusTap(String likedUserId, String likedPostId,String followOpusId,String followOpusTap);
}
