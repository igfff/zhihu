package com.zhihu.dao;

import com.zhihu.pojo.LikeOpus;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface LikeOpusRepository extends JpaRepository<LikeOpus,Integer> {

    List<LikeOpus> findByLikedPostIdAndFollowOpusTapAndLikeStatus(String likedPostId, String followOpusTap, Integer likeStatus);
}
