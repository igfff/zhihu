package com.zhihu.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "zhihu-edu-opusCommNums",path = "/opn")
public interface OpnClient {

    //将作品类型ID和Id拼接作为key值添加到redis中，并且初始化value评论数为0
    @RequestMapping(value = "/insertKeyToRedis",method = RequestMethod.POST)
    public void insertKeyToRedis(@RequestParam("opusTap")Integer opusTap, @RequestParam("opusId")Integer opusId);
}
