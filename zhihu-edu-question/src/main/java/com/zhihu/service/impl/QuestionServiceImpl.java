package com.zhihu.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.zhihu.client.LikeClient;
import com.zhihu.client.OpnClient;
import com.zhihu.dao.QuestionRepository;
import com.zhihu.dao.QuestionReqDao;
import com.zhihu.dao.TypeTempDao;
import com.zhihu.pojo.*;
import com.zhihu.pojo.req.QuestionReq;
import com.zhihu.pojo.baseVo.BaseResult;
import com.zhihu.service.QuestionService;
import com.zhihu.utils.HeaderUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class QuestionServiceImpl implements QuestionService {

    @Autowired
    QuestionReqDao questionReqDao;

    @Autowired
    HeaderUtils headerUtils;

    @Autowired
    TypeTempDao typeTempDao;

    @Autowired
    QuestionRepository questionRepository;

    @Autowired
    OpnClient opnClient;

    @Autowired
    LikeClient likeClient;

    @Override
    public BaseResult InsertQuestion(QuestionReq questionReq, HttpServletRequest request) {

        BaseResult baseResult = new BaseResult();
        //从token中获取到用户id
        String userJson = headerUtils.getUserJson(request);
        //用户Id搜索到用户
        User user = JSONObject.parseObject(userJson, User.class);

        //判断用户是否登录
        if (user!=null) {
            //Question question = new Question();
            TypeTemp typeTemp = new TypeTemp();
            //List<Type> respTypelist = new ArrayList<Type>();
            //QuestionReq questionReq1 = new QuestionReq();

            questionReq.setUserId(user.getUserId());
            questionReq.setQuesUserName(user.getUserName());
            questionReq.setQuesUserImg(user.getImg());
            //作品如果是问题，类型指定为1；
            questionReq.setOpusTap(1);
            //问题评论数默认为0
            questionReq.setQuesCommNums(0);
            //问题的点赞数默认为0
            questionReq.setQuesLikeNum(0);
            questionReq.setQuesCreatetime(new Date());
            //将前端接收的questionReq实体转换为question
            //BeanUtils.copyProperties(questionReq,question);
            //添加问题到数据库中
            int i = questionReqDao.InsertQuestion(questionReq);
            //获取到问题绑定的标签集合
            List<Type> typesList = questionReq.getTypesList();
            if (i>0){
                //添加数据库成功后，将发布的作品类型和名字作为Key值，添加到redis中，并将点赞数和评论数赋值为0
                opnClient.insertKeyToRedis(questionReq.getOpusTap(),questionReq.getQuestionId());
                likeClient.insertOpusLikeNums(questionReq.getOpusTap(),questionReq.getQuestionId());
                //遍历标签集合，添加到type_temp的中间关系表中
                for (Type type:typesList) {
                    typeTemp.setTtOpus(questionReq.getQuestionId());
                    typeTemp.setTtOpusTap(1);
                    typeTemp.setTypeId(type.getTypeId());
                    int m = typeTempDao.insertTypeTemp(typeTemp);
                    //每添加成功一个标签，就往respTypelist集合中添加一个标签对象
                    //if (m>0){
                    //    respTypelist.add(type);
                    //}
                }
                //再通过问题id查询到数据库中的问题
               QuestionReq questionById = questionReqDao.findQuestionById(questionReq.getQuestionId());

                //BeanUtils.copyProperties(questionById,questionReq1);
                //给问题加标签集合
                //questionReq1.setTypesList(respTypelist);

                baseResult.setCode(0);
                baseResult.setMsg("问题提交成功！");
                baseResult.setData(questionById);
                return baseResult;
            }
            baseResult.setCode(1);
            baseResult.setMsg("问题提交失败！");
            return baseResult;
        }
        baseResult.setCode(1);
        baseResult.setMsg("请先登录！");
        return baseResult;
    }

    @Override
    public BaseResult findQuestionById(Integer questionId) {
        BaseResult baseResult = new BaseResult();
        if (questionId!=null){
            QuestionReq questionById = questionReqDao.findQuestionById(questionId);
            baseResult.setCode(0);
            baseResult.setMsg("查询成功");
            baseResult.setData(questionById);
            return baseResult;
        }
        baseResult.setCode(1);
        baseResult.setMsg("查询失败");
        return baseResult;
    }

    @Override
    public BaseResult findAllQuestion() {
        BaseResult baseResult = new BaseResult();
        List<QuestionReq> allQuestion = questionReqDao.findAllQuestion();
        if (allQuestion!=null){
            baseResult.setCode(0);
            baseResult.setMsg("查询成功");
            baseResult.setData(allQuestion);
            return baseResult;
        }
        baseResult.setCode(1);
        baseResult.setMsg("还没有用户提问嗷！您来提问一下吧！");
        return baseResult;
    }

    @Override
    public BaseResult addQuestionCommNums(Integer questionId) {
        BaseResult baseResult = new BaseResult();
        int i = questionReqDao.addQuestionCommNums(questionId);
        if (i>0){
            baseResult.setCode(0);
            baseResult.setMsg("评论数增加1");
            return baseResult;
        }
        baseResult.setCode(1);
        baseResult.setMsg("评论数增加失败");
        return baseResult;
    }

    @Override
    public BaseResult findQuestionsByUserId(Integer userId) {
        BaseResult baseResult = new BaseResult();
        List<QuestionReq> questionsByUserId = questionReqDao.findQuestionsByUserId(userId);
        if (questionsByUserId!=null){
            baseResult.setCode(0);
            baseResult.setMsg("查询成功");
            baseResult.setData(questionsByUserId);
            return baseResult;
        }
        baseResult.setCode(1);
        baseResult.setMsg("用户未发表任何问题！");
        return baseResult;
    }

    @Override
    public BaseResult findFavorQuesByUserId(Integer userId) {
        BaseResult baseResult = new BaseResult();
        List<QuestionReq> favorQuesByUserId = questionReqDao.findFavorQuesByUserId(userId);
        if (favorQuesByUserId!=null){
            baseResult.setCode(0);
            baseResult.setMsg("查询成功");
            baseResult.setData(favorQuesByUserId);
            return baseResult;
        }
        baseResult.setCode(1);
        baseResult.setMsg("用户未关注任何问题！");
        return baseResult;
    }

    @Override
    public BaseResult findQuesByTypeId(Integer typeId) {
        BaseResult baseResult = new BaseResult();
        List<QuestionReq> quesByTypeId = questionReqDao.findQuesByTypeId(typeId);
        List<QuestionReq> quesList = new ArrayList<QuestionReq>();
        if (quesByTypeId!=null){
            for (QuestionReq ques:quesByTypeId) {
                QuestionReq questionById = questionReqDao.findQuestionById(ques.getQuestionId());
                quesList.add(questionById);
            }

            baseResult.setCode(0);
            baseResult.setMsg("查询成功");
            baseResult.setData(quesList);
            return baseResult;
        }
        baseResult.setCode(1);
        baseResult.setMsg("当前标签下没有问题！");
        return baseResult;
    }

    @Override
    public BaseResult updateQuestion(Question question) {
        BaseResult baseResult = new BaseResult();
        if (question!=null) {
            questionRepository.saveAndFlush(question);
            baseResult.setCode(0);
            baseResult.setMsg("更新数据成功！");
            return baseResult;
        }
        baseResult.setCode(1);
        baseResult.setMsg("更新数据失败！");
        return baseResult;
    }

}
