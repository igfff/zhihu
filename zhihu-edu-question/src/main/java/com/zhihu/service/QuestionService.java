package com.zhihu.service;

import com.zhihu.pojo.Question;
import com.zhihu.pojo.req.QuestionReq;
import com.zhihu.pojo.baseVo.BaseResult;

import javax.servlet.http.HttpServletRequest;

public interface QuestionService {
    BaseResult InsertQuestion(QuestionReq questionReq, HttpServletRequest request);

    BaseResult findQuestionById(Integer questionId);

    BaseResult findAllQuestion();

    BaseResult addQuestionCommNums(Integer questionId);

    BaseResult findQuestionsByUserId(Integer userId);

    BaseResult findFavorQuesByUserId(Integer userId);

    BaseResult findQuesByTypeId(Integer typeId);

    BaseResult updateQuestion(Question question);

}
