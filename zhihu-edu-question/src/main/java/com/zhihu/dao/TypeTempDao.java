package com.zhihu.dao;

import com.zhihu.pojo.TypeTemp;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface TypeTempDao {

    int insertTypeTemp(TypeTemp typeTemp);
}
