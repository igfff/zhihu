package com.zhihu.dao;

import com.zhihu.pojo.req.QuestionReq;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface QuestionReqDao {

    int InsertQuestion(QuestionReq questionReq);

    QuestionReq findQuestionById(@Param("questionId") Integer questionId);

    List<QuestionReq> findAllQuestion();

    int addQuestionCommNums(@Param("questionId")Integer questionId);

    List<QuestionReq> findQuestionsByUserId(@Param("userId")Integer userId);

    List<QuestionReq> findFavorQuesByUserId(@Param("userId")Integer userId);

    List<QuestionReq> findQuesByTypeId(@Param("typeId")Integer typeId);

    int updateQuestion(QuestionReq questionReq);
}
