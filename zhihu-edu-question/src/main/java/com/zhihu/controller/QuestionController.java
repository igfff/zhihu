package com.zhihu.controller;

import com.zhihu.pojo.Question;
import com.zhihu.pojo.req.QuestionReq;
import com.zhihu.pojo.baseVo.BaseResult;
import com.zhihu.service.QuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RequestMapping("/question")
@RestController
public class QuestionController {

    @Autowired
    QuestionService questionService;


    //提交问题
    @RequestMapping(value = "/InsertQuestion",method = RequestMethod.POST)
    public BaseResult InsertQuestion(@RequestBody QuestionReq questionReq, HttpServletRequest request){
        return questionService.InsertQuestion(questionReq,request);
    }

    //通过问题id查询问题
    @RequestMapping(value = "/findQuestionById",method = RequestMethod.GET)
    public BaseResult findQuestionById(@RequestParam("questionId") Integer questionId){
        return questionService.findQuestionById(questionId);
    }

    //查询所有问题
    @RequestMapping(value = "/findAllQuestion",method = RequestMethod.POST)
    public BaseResult findAllQuestion(){
        return questionService.findAllQuestion();
    }

    //问题评论数增长
    @RequestMapping(value = "/addQuestionCommNums",method = RequestMethod.GET)
    public BaseResult addQuestionCommNums(@RequestParam("questionId") Integer questionId){
        return questionService.addQuestionCommNums(questionId);
    }

    //通过用户id查询所有发过的问题，并按提交时间排序
    @RequestMapping(value = "/findQuestionsByUserId",method = RequestMethod.GET)
    public BaseResult findQuestionsByUserId(@RequestParam("userId")Integer userId){
        return questionService.findQuestionsByUserId(userId);
    }

    //通过用户id查询所有关注的问题，并按关注时间排序
    @RequestMapping(value = "/findFavorQuesByUserId",method = RequestMethod.GET)
    public BaseResult findFavorQuesByUserId(@RequestParam("userId")Integer userId){
        return questionService.findFavorQuesByUserId(userId);
    }

    //通过标签id查询所有绑定标签的问题
    @RequestMapping(value = "/findQuesByTypeId",method = RequestMethod.GET)
    public BaseResult findQuesByTypeId(@RequestParam("typeId")Integer typeId){
        return questionService.findQuesByTypeId(typeId);
    }

    //通过传过来的实体更新question数据库信息
    @RequestMapping(value = "/updateQuestion",method = RequestMethod.POST)
    public BaseResult updateQuestion(@RequestBody Question question){
        return questionService.updateQuestion(question);
    }

}
