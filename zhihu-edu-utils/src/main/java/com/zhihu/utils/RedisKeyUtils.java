package com.zhihu.utils;

public class RedisKeyUtils {

    //保存用户点赞作品数据的key
    public static final String MAP_USER_OPUS_LIKED = "MAP_USER_OPUS_LIKED";

/*    //保存用户点赞评论数据的key
    public static final String MAP_USER_COMM_LIKED = "MAP_USER_COMM_LIKED";*/

    //保存作品的点赞数量的的key：value值存的是点赞数量
    public static final String MAP_USER_OPUS_COUNT = "MAP_USER_OPUS_COUNT";

    //保存已发布作品的key值：value值存的是评论数量
    public static final String MAP_OPUS_KEY = "MAP_OPUS_KEY";

    //保存热门排行榜问题的key
    public static final String MAP_HOT_QUESTION = "MAP_HOT_QUESTION";

/*    //保存用户被点赞评论数量的key
    public static final String MAP_USER_COMM_COUNT = "MAP_USER_COMM_COUNT";*/

    /**
     * 拼接被点赞的用户id和点赞的人的id作为key。格式 222222::333333
     *
     * @param likedUserId 被点赞的人id
     * @param likedPostId 点赞的人的id
     * @return
     */
    public static String getLikedOpusKey(String likedUserId, String likedPostId,String followOpusId,String followOpusTap) {
        StringBuilder builder = new StringBuilder();
        builder.append(likedUserId);
        builder.append("::");
        builder.append(likedPostId);
        builder.append("::");
        builder.append(followOpusId);
        builder.append("::");
        builder.append(followOpusTap);
        return builder.toString();
    }

/*    public static String getLikedCommKey(String likedUserId, String likedPostId,Integer lcCommentId) {
        StringBuilder builder = new StringBuilder();
        builder.append(likedUserId);
        builder.append("::");
        builder.append(likedPostId);
        builder.append("::");
        builder.append(lcCommentId);
        return builder.toString();
    }*/
    //
    public static String getLikedOpusCountKey(String followOpusTap,String followOpusId){
        StringBuilder builder = new StringBuilder();
        builder.append(followOpusTap);
        builder.append("::");
        builder.append(followOpusId);
        return builder.toString();
    }

/*    public static String getLikedCommsCountKey(String commentsId){
        StringBuilder builder = new StringBuilder();
        builder.append(commentsId);
        return builder.toString();
    }*/

    //作品类型::作品id拼接成 redis的二层Key
    public static String getPublishedOpusKey(String opusTap,String opusId){
        StringBuilder builder = new StringBuilder();
        builder.append(opusTap);
        builder.append("::");
        builder.append(opusId);
        return builder.toString();
    }
}
