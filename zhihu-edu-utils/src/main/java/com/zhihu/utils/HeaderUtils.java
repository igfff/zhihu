package com.zhihu.utils;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

@Component
public class HeaderUtils {

//    public static TbUser getUser(HttpServletRequest request){
//        Cookie[] cookies = request.getCookies();
//        String token=null;
//        if (cookies!=null){
//            for (Cookie coo:cookies
//            ) {
//                if (coo.getName().equals("token")){
//                    token=coo.getValue();
//                }
//            }
//        }
//        if (token==null){
//            return null;
//        }
//        HttpSession session = request.getSession();
//        TbUser attribute =(TbUser) session.getAttribute(token);
//
//        return attribute;
//    }
//
//    //更新session中的值
//    public static void updateSession(TbUser tbUser,HttpServletRequest request){
//        Cookie[] cookies = request.getCookies();
//        String token=null;
//        if (cookies!=null){
//            for (Cookie coo:cookies
//            ) {
//                if (coo.getName().equals("token")){
//                    token=coo.getValue();
//                }
//            }
//        }
//        HttpSession session = request.getSession();
//        session.setAttribute(token,tbUser);
//
//    }
        @Autowired
        RedisUtils redisUtils;
        public String getUserJson(HttpServletRequest request){
            String token = request.getHeader("token");
            String jwtToken = redisUtils.get(token).toString();
            JwtUtils jwtUtils = new JwtUtils();
            String s = jwtUtils.VerifyToekn(jwtToken);
            return s;
        }
}
