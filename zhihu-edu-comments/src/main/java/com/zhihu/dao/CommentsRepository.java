package com.zhihu.dao;

import com.zhihu.pojo.Comments;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CommentsRepository extends JpaRepository<Comments,Integer> {
    List<Comments> findByFollowOpusTapAndFollowOpusId(Integer followOpusTap, Integer followOpusId);
}
