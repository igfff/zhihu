package com.zhihu.dao;

import com.zhihu.pojo.Comments;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface CommentsDao {

    List<Comments> findCommentsTimeSort(@Param("followOpusTap") Integer followOpusTap, @Param("followOpusId")Integer followOpusId);

    List<Comments> findCommentsLikeNumSort(@Param("followOpusTap")Integer followOpusTap,@Param("followOpusId") Integer followOpusId);
}
