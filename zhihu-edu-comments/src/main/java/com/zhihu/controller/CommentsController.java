package com.zhihu.controller;

import com.zhihu.pojo.Comments;
import com.zhihu.pojo.baseVo.BaseResult;
import com.zhihu.service.CommentsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RequestMapping("/comments")
@RestController
public class CommentsController {

    @Autowired
    CommentsService commentsService;

    //添加评论，需要字段：评论内容；被评论用户id；被评论用户的名字；被评论用户的头像
    //被评论作品/评论的类型opusTap;作品/评论的id
    @RequestMapping(value = "/insertComments",method = RequestMethod.POST)
    public BaseResult insertComments(@RequestBody Comments comments, HttpServletRequest request){
        return commentsService.insertComments(comments,request);
    }

    //根据作品/评论的id来查询对应的评论，按照时间顺序排序（越晚的排的越靠前）
    @RequestMapping(value = "/findCommentsTimeSort",method = RequestMethod.GET)
    public BaseResult findCommentsTimeSort(@RequestParam("followOpusTap")Integer followOpusTap,@RequestParam("followOpusId")Integer followOpusId){
        return commentsService.findCommentsTimeSort(followOpusTap,followOpusId);
    }

    //根据作品/评论的id来查询对应的评论，按照点赞顺序排序（越早的排的越靠前）
    @RequestMapping(value = "/findCommentsLikeNumSort",method = RequestMethod.GET)
    public BaseResult findCommentsLikeNumSort(@RequestParam("followOpusTap")Integer followOpusTap,@RequestParam("followOpusId")Integer followOpusId){
        return commentsService.findCommentsLikeNumSort(followOpusTap,followOpusId);
    }
}
