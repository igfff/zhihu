package com.zhihu.service;

import com.zhihu.pojo.Comments;
import com.zhihu.pojo.baseVo.BaseResult;

import javax.servlet.http.HttpServletRequest;

public interface CommentsService {
    BaseResult insertComments(Comments comments, HttpServletRequest request);

    BaseResult findCommentsTimeSort(Integer followOpusTap, Integer followOpusId);

    BaseResult findCommentsLikeNumSort(Integer followOpusTap, Integer followOpusId);
}
