package com.zhihu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * @author : zhangzheng
 * ClassName: ZhiHuEduEurekApplication
 * @date : 2021/9/2 20:05
 */
@SpringBootApplication
@EnableEurekaServer
public class ZhiHuEduEurekApplication {
    public static void main(String[] args) {
        SpringApplication.run(ZhiHuEduEurekApplication.class);
    }
}
