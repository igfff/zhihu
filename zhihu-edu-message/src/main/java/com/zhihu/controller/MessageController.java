package com.zhihu.controller;

import com.zhihu.pojo.baseVo.BaseResult;
import com.zhihu.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * @author : zhangzheng
 * ClassName: MessageController
 * @date : 2021/9/3 16:25
 */
@RestController
@RequestMapping("/message")
public class MessageController {
    /**
     * 短信验证
     */
    @Autowired
    MessageService messageService;

    @RequestMapping(value = "/sendMsg",method = RequestMethod.POST)
    public BaseResult sendMsg(@RequestParam("telephone") String telephone){
       return messageService.sendMsg(telephone);
    }
}
