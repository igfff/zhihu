package com.zhihu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author : zhangzheng
 * ClassName: ZhiHuMessageApplication
 * @date : 2021/9/3 14:27
 */
@SpringBootApplication
@EnableDiscoveryClient
public class ZhiHuMessageApplication {
    public static void main(String[] args) {
        SpringApplication.run(ZhiHuMessageApplication.class);
    }
}
