package com.zhihu.service;

import com.zhihu.pojo.baseVo.BaseResult;
import org.springframework.data.repository.query.Param;

/**
 * @author : zhangzheng
 * ClassName: MessageService
 * @date : 2021/9/3 16:26
 */
public interface MessageService {
    BaseResult sendMsg(@Param("telephone") String telephone);
}
