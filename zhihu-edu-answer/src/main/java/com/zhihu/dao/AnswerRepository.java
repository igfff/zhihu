package com.zhihu.dao;

import com.zhihu.pojo.Answer;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AnswerRepository extends JpaRepository<Answer,Integer> {
    Answer findByAnswerId(Integer answerId);

    List<Answer> findByQuestionId(Integer questionId);

}
