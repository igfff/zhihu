package com.zhihu.dao;

import com.zhihu.pojo.req.AnswerReq;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface AnswerReqDao {

    List<AnswerReq> findAnswersByQuestionId(@Param("questionId")Integer questionId);

    int addAnswers(AnswerReq answerReq);

    AnswerReq findAnswerByAnswerId(@Param("answerId")Integer answerId);

    Long findAnswersCount(@Param("questionId")Integer questionId);
}
