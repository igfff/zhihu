package com.zhihu.controller;

import com.zhihu.pojo.Answer;
import com.zhihu.pojo.Question;
import com.zhihu.pojo.baseVo.BaseResult;
import com.zhihu.pojo.req.AnswerReq;
import com.zhihu.service.AnswerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RequestMapping("/answer")
@RestController
public class AnswerController {

    @Autowired
    AnswerService answerService;

    //通过问题id查询该问题所有回答,并按点赞数量从高到低排序
    @RequestMapping(value = "/findAnswersByQuestionId",method = RequestMethod.GET)
    public BaseResult findAnswersByQuestionId(@RequestParam("questionId")Integer questionId){
        return answerService.findAnswersByQuestionId(questionId);
    }

    //根据问题id添加回答，需要前端传入问题id和问题标题
    @RequestMapping(value = "/addAnswers",method = RequestMethod.POST)
    public BaseResult addAnswers(@RequestBody AnswerReq answerReq, HttpServletRequest request){
        return answerService.addAnswers(answerReq,request);
    }

    //根据回答的id查询回答
    @RequestMapping(value = "/findAnswerById",method = RequestMethod.GET)
    public BaseResult findAnswerById(@RequestParam("answerId") Integer answerId){
        return answerService.findAnswerById(answerId);
    }

    //通过传过来的实体更新answer数据库信息
    @RequestMapping(value = "/updateAnswer",method = RequestMethod.POST)
    public BaseResult updateAnswer(@RequestBody Answer answer){
        return answerService.updateAnswer(answer);
    }
}
