package com.zhihu.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "zhihu-edu-like",path = "/like")
public interface LikeClient {

    //将作品类型Id和Id拼接作为key值添加到redis中，并且初始化value点赞数为0
    @RequestMapping(value = "/insertOpusLikeNums",method = RequestMethod.GET)
    public void insertOpusLikeNums(@RequestParam("followOpusTap") Integer followOpusTap, @RequestParam("followOpusId") Integer followOpusId);
}
