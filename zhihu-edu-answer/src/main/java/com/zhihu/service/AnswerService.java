package com.zhihu.service;

import com.zhihu.pojo.Answer;
import com.zhihu.pojo.baseVo.BaseResult;
import com.zhihu.pojo.req.AnswerReq;

import javax.servlet.http.HttpServletRequest;

public interface AnswerService {

    BaseResult findAnswersByQuestionId(Integer questionId);

    BaseResult addAnswers(AnswerReq answerReq, HttpServletRequest request);

    BaseResult findAnswerById(Integer answerId);

    BaseResult updateAnswer(Answer answer);
}
